package com.spring.controller;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spring.common.Pagination;
import com.spring.model.Bookmark;
import com.spring.model.User;
import com.spring.service.BookmarkService;

@Controller
public class BookmarkController {

	@Autowired
	private BookmarkService bookmarkService;
	
	/**
	 * 북마크 페이지로 이동
	 * @return
	 */
	@RequestMapping("/getBookmarkListView.do")
	public String getBookmarkListView() {
		return "index.jsp?content=bookmark/getBookmarkList";
	}
	
	/**
	 * 북마크 리스트를 가져온다.
	 * @param pagination
	 * @param session
	 * @return
	 */
	@RequestMapping("/getBookmarkList.do")
	public ModelAndView getBookmarkList(Pagination pagination, HttpSession session) {
		// 세션에서 로그인한 회원의 정보를 가져온다
		User login = (User)session.getAttribute("login");
		// 회원 아이디를 pagination에 set하여 setCriterion() 수행
		pagination.setCriterion(login.getId());
		// getBookmarkList()수행하여 리턴 값을 HashMap에 담기
		HashMap<String, Object> map = bookmarkService.getBookmarkList(pagination);
		// login 정보 map에 put
		map.put("login", login);
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 북마크를 등록한다.
	 * @param bookmark
	 * @param session
	 * @return
	 */
	@RequestMapping("/insertBookmark.do")
	public ModelAndView insertBookmark(Bookmark bookmark, HttpSession session) {
		User login = (User)session.getAttribute("login");
		bookmark.setUser_id(login.getId());
		HashMap<String, Object> map = bookmarkService.insertBookmark(bookmark);
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 북마크를 수정한다.
	 * @param bookmark
	 * @return
	 */
	@RequestMapping("/updateBookmark.do")
	public ModelAndView updateBookmark(Bookmark bookmark) {
		HashMap<String, Object> map = bookmarkService.updateBookmark(bookmark);
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 북마크를 삭제한다.
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteBookmark.do")
	public ModelAndView deleteBookmark(int id) {
		HashMap<String, Object> map = bookmarkService.deleteBookmark(id);
		return new ModelAndView("jsonView", map);
	}
	
}
