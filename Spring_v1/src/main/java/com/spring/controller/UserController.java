package com.spring.controller;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.model.User;
import com.spring.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	/**
	 * 내정보 페이지로 이동한다.
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping("/getUser.do")
	public String getUser(HttpSession session, Model model) {
		// 세션에서 로그인한 회원을 확인한다.
		User login = (User)session.getAttribute("login");
		// getUser()로 회원 정보를 가져오고 HashMap에 담는다.
		HashMap<String, Object> map = userService.getUser(login.getId());
		// 회원정보는 model에 담아 전달
		model.addAttribute("user", map.get("user"));
		// getUser 페이지로 이동한다.
		return "index.jsp?content=user/getUser";
	}
	
	/**
	 * 회원가입 페이지로 이동한다.
	 * @return
	 */
	@RequestMapping(value="/insertUser.do", method=RequestMethod.GET)
	public String insertUserView() {
		return "index.jsp?content=user/insertUser";
	}
	
	/**
	 * 회원을 등록한다.
	 * @param user
	 * @return
	 */
	@RequestMapping(value="/insertUser.do", method=RequestMethod.POST)
	public ModelAndView insertUser(User user) {
		// insertUser() 수행하고 리턴 값을 HashMap에 담는다. 
		HashMap<String, Object> map = userService.insertUser(user);
		// ModelAndView 객체 생성하여 리턴한다.
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 회원 정보를 수정한다.
	 * @param user
	 * @param session
	 * @return
	 */
	@RequestMapping("/updateUser.do")
	public ModelAndView updateUser(User user, HttpSession session) {
		// 세션에서 로그인한 회원을 확인한다.
		User login = (User)session.getAttribute("login");
		// 넘겨 받은 user에 로그인한 회원 id로 set
		user.setId(login.getId());
		// updateUser()를 수행하고 map에 리턴 값을 담는다.
		HashMap<String, Object> map = userService.updateUser(user);
		// ModelAndView 객체를 생성하여 리턴한다.
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 회원 삭제를 수행한다.
	 * @param session
	 * @return
	 */
	@RequestMapping("/deleteUser.do")
	public ModelAndView deleteUser(HttpSession session) {
		// 세션으로 로그인한 회원을 확인한다.
		User login = (User)session.getAttribute("login");
		// deleteUser()를 수행하고 리턴 값을 HashMap에 담는다.
		HashMap<String, Object> map = userService.deleteUser(login.getId());
		// session을 무효화한다.
		session.invalidate();
		// ModelAndView 객체를 생성하여 리턴한다.
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 이메일, 닉네임이 사용 가능한지 확인한다.
	 * @param condition
	 * @param keyword
	 * @param session
	 * @return
	 */
	@RequestMapping("/checkUser.do")
	public ModelAndView checkUser(String condition, String keyword, HttpSession session) {
		// 세션에서 로그인 회원 정보를 가져온다.
		User login = (User)session.getAttribute("login");
		int id = 0;
		if(login != null) {
			// 회원이 로그인한 상태라면 회원 아이디를 id에 담는다.
			id = login.getId();
		}
		// checkUser()를 수행하고 리턴 값은 HashMap에 담는다. 
		HashMap<String, Object> map = userService.checkUser(condition, keyword, id);
		// ModelAndView 객체를 생성해서 리턴한다.
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 비밀번호를 맞게 입력했는지 확인한다.	
	 * @param password
	 * @param session
	 * @return
	 */
	@RequestMapping("/checkPassword.do")
	public ModelAndView checkPassword(String password, HttpSession session) {
		// 세션에서 로그인한 회원의 정보를 가져온다.
		User login = (User)session.getAttribute("login");
		// 로그인한 회원의 id를 id에 담는다.
		int id = login.getId();
		// checkPassword()를 수행하고 리턴 값은 HashMap에 담는다.
		HashMap<String, Object> map = userService.checkPassword(id, password);
		// ModelAndView 객체를 생성해서 리턴한다.
		return new ModelAndView("jsonView", map);
	}
}
