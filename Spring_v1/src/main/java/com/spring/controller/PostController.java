package com.spring.controller;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.spring.common.Pagination;
import com.spring.model.Post;
import com.spring.model.User;
import com.spring.service.PostService;


@Controller
public class PostController {

	@Autowired
	private PostService postService;
	
	/**
	 * post 정보를 조회하고, getPost 페이지를 표시한다. 
	 * @param post
	 * @param model
	 * @param session
	 * @return
	 */
	@RequestMapping("/getPost.do")
	public String getPost(int id, Pagination pagination, Model model) {
		model.addAttribute("post", postService.getPost(id));
		model.addAttribute("pagination", pagination);
		return "index.jsp?content=post/getPost";
	}
	
	/**
	 * post를 읽을 수 있는지 확인한다.
	 * @param id
	 * @param session
	 * @return
	 */
	@RequestMapping("/checkPost.do")
	public ModelAndView checkPost(int id, HttpSession session) {
		// 세션에서 로그인 정보를 가져오고
		User login = (User)session.getAttribute("login");
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(login != null) {	// 로그인 정보가 있다면
			// checkPost()를 수행해서 map에 담는다.
			map = postService.checkPost(id, login);	
		} else {	// 로그인 정보가 없다면
			map.put("result", "failure");
			map.put("msg", "비회원은 읽을 수 없습니다");
		}
		return new ModelAndView("jsonView",map);
	}
	
	/**
	 * postList를 조회하고 getPostList를 표시한다.
	 * @param post
	 * @param model
	 * @param curPage
	 * @return
	 */
	@RequestMapping("/getPostList.do")
	public String getPostList(Pagination pagination, Model model) {
		HashMap<String, Object> map = postService.getPostList(pagination);
		model.addAttribute("postList", map.get("postList"));
		model.addAttribute("pagination", map.get("pagination"));
		return "index.jsp?content=post/getPostList";
	}
	
	/**
	 * 글쓰기 페이지로 이동한다.
	 * @param category_id
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/insertPost.do", method=RequestMethod.GET)
	public String insertPostView(int category_id, @RequestParam(value="parent_id", defaultValue="0")int parent_id, Model model) {
		model.addAttribute("category_id", category_id);
		model.addAttribute("parent_id", parent_id);
		return "index.jsp?content=post/insertPost";
	}
	
	/**
	 * post를 등록하고, 해당 카테고리 목록 페이지로 이동한다.
	 * @param post
	 * @return
	 */
	@RequestMapping(value="/insertPost.do", method=RequestMethod.POST)
	public ModelAndView insertPost(Post post, HttpSession session) {
		// 세션에서 로그인한 회원 정보를 가져온다.
		User login = (User)session.getAttribute("login");
		// 로그인한 회원의 id를 post의 user_id로 set
		post.setUser_id(login.getId());
		// insertPost()를 수행하고 리턴 값을 HashMap에 담는다.
		HashMap<String, Object> map = postService.insertPost(post);
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * post 수정 페이지로 이동한다.
	 * @param post
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/updatePost.do", method=RequestMethod.GET)
	public String updatePostView(int id, Model model) {
		HashMap<String, Object> map = postService.updatePostView(id);
		model.addAttribute("post", map.get("post"));
		return "index.jsp?content=post/updatePost";
	}
	
	/**
	 * post를 수정하고 getPost 페이지로 이동한다.
	 * @param post
	 * @return
	 */
	@RequestMapping(value="/updatePost.do", method=RequestMethod.POST)
	public String updatePost(Post post) {
		postService.updatePost(post);
		return "redirect: getPost.do?id=" + post.getId()
				+ "&criterion=" + post.getCategory_id()
				+ "&curPage=" + 1;
	}
	
	/**
	 * post를 삭제한다.
	 * @param post
	 * @return
	 */
	@RequestMapping("/deletePost.do")
	public String deletePost(int id, int category_id) {
		postService.deletePost(id);
		return "redirect: getPostList.do?criterion=" + category_id;
	}

}
