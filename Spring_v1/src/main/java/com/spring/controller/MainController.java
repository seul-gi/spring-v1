package com.spring.controller;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.model.User;
import com.spring.service.UserService;

@Controller
public class MainController {

	@Autowired
	private UserService userService;

	/**
	 * 처음 접속하면 이동할 페이지
	 * @return
	 */
	@RequestMapping("/main.do")
	public String mainDo() {
		return "redirect: getPostList.do?criterion=1";
	}
	
	/**
	 * 로그인 페이지로 이동한다.
	 * @return
	 */
	@RequestMapping(value="/login.do", method=RequestMethod.GET)
	public String loginView() {
		return "index.jsp?content=user/login";
	}
	
	/**
	 * 로그인을 수행한다.	
	 * @param user
	 * @param session
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/login.do", method=RequestMethod.POST)
	public ModelAndView login(User user, HttpSession session, Model model) {
		HashMap<String, Object> map = userService.login(user);
		if(map.get("result").equals("success")) {	// 로그인에 성공하면
			// 세션에 로그인 정보를 담는다.
			session.setAttribute("login", map.get("login"));
		}
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 로그아웃을 수행한다.
	 * @param session
	 * @return
	 */
	@RequestMapping("/logout.do")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect: main.do";
	}
	
}
