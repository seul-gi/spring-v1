package com.spring.controller;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spring.common.Pagination;
import com.spring.model.Comment;
import com.spring.model.User;
import com.spring.service.CommentService;


@Controller
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	/**
	 * 댓글 리스트를 조회한다.
	 * @param pagination
	 * @param session
	 * @return
	 */
	@RequestMapping("/getCommentList.do")
	public ModelAndView getCommentList(Pagination pagination, HttpSession session) {
		HashMap<String, Object> map = commentService.getCommentList(pagination);
		if(map.get("result").equals("success")) {	// 댓글 리스트를 가져왔다면
			// 세션에서 로그인 정보를 가져와서 map에 put
			User login = (User)session.getAttribute("login");
			map.put("login", login);
		} 
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 댓글을 등록한다.
	 * @param comment
	 * @param session
	 * @return
	 */
	@RequestMapping("/insertComment.do")
	public ModelAndView insertComment(Comment comment, HttpSession session) {
		User login = (User)session.getAttribute("login");
		// 로그인 정보의 id를 comment에 set
		comment.setUser_id(login.getId());
		HashMap<String, Object> map = commentService.insertComment(comment);
		return new ModelAndView("jsonView", map);
	}
	
	/**
	 * 댓글을 수정한다.
	 * @param comment
	 * @return
	 */
	@RequestMapping("/updateComment.do")
	public ModelAndView updateComment(Comment comment) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(commentService.updateComment(comment) == 1) {
			map.put("result", "success");
		}
		return new ModelAndView("jsonView", map);
	}
	
	
	/**
	 * 댓글을 삭제한다.
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteComment.do")
	public ModelAndView deleteComment(int id) {
		HashMap<String, Object> map = commentService.deleteComment(id);
		return new ModelAndView("jsonView", map);
	}
	
}
