package com.spring.model;

import java.sql.Date;

public class Comment {

	private int id;
	private int post_id;
	private int user_id;
	private String writer;
	private String content;
	private int group_id;
	private int parent_id;
	private int sequence;
	private int depth;
	private int children;
	private int post_user;
	private int parent_user;
	private int secret;
	private Date reg_date;
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public int getChildren() {
		return children;
	}
	public void setChildren(int children) {
		this.children = children;
	}
	public int getPost_user() {
		return post_user;
	}
	public void setPost_user(int post_user) {
		this.post_user = post_user;
	}
	public int getParent_user() {
		return parent_user;
	}
	public void setParent_user(int parent_user) {
		this.parent_user = parent_user;
	}
	public int getSecret() {
		return secret;
	}
	public void setSecret(int secret) {
		this.secret = secret;
	}
	public Date getReg_date() {
		return reg_date;
	}
	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Comment [id=" + id + ", post_id=" + post_id + ", user_id=" + user_id + ", writer=" + writer
				+ ", content=" + content + ", group_id=" + group_id + ", parent_id=" + parent_id + ", sequence="
				+ sequence + ", depth=" + depth + ", children=" + children + ", post_user=" + post_user
				+ ", parent_user=" + parent_user + ", secret=" + secret + ", reg_date=" + reg_date + ", status="
				+ status + "]";
	}
	
}
