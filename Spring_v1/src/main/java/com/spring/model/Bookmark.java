package com.spring.model;

import java.sql.Date;

public class Bookmark {

	private int id;
	private int user_id;
	private int post_id;
	private int post_category;
	private String title;
	private Date reg_date;
	private int status;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	public int getPost_category() {
		return post_category;
	}
	public void setPost_category(int post_category) {
		this.post_category = post_category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getReg_date() {
		return reg_date;
	}
	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Bookmark [id=" + id + ", user_id=" + user_id + ", post_id=" + post_id + ", post_category="
				+ post_category + ", title=" + title + ", reg_date=" + reg_date + ", status=" + status + "]";
	}
	
}
