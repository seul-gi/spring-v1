package com.spring.model;

import java.sql.Date;

public class Category {
	
	private int id;
	private String subject;
	private int status;
	private Date reg_date;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getReg_date() {
		return reg_date;
	}
	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}
	
	@Override
	public String toString() {
		return "Category [id=" + id + ", subject=" + subject + ", status=" + status + ", reg_date=" + reg_date + "]";
	}
	
	

}
