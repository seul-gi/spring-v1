package com.spring.common;



public class Pagination {
	
	// 한 페이지당 글 수
	private int pageSize;
	
	// 한 블록당 페이지 수
	private int blockSize;
	
	// 현재 페이지
	private int curPage;

	// 레코드 시작점
	private int startPoint;

	// 전체 글 수
	private int modelCnt;
	
	// 전체 페이지 수
	private int pageCnt;
	
	// 시작 페이지
	private int startPage;
	
	// 마지막 페이지
	private int endPage;
	
	// 이전 페이지
	private int prePage;
	
	// 다음 페이지
	private int nextPage;

	// 기준
	private int criterion;
	
	// 검색 조건
	private String searchCondition;
	
	// 검색어
	private String searchKeyword;

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public int getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(int startPoint) {
		this.startPoint = startPoint;
	}

	public int getModelCnt() {
		return modelCnt;
	}

	public void setModelCnt(int modelCnt) {
		this.modelCnt = modelCnt;
	}

	public int getPageCnt() {
		return pageCnt;
	}

	public void setPageCnt(int pageCnt) {
		this.pageCnt = pageCnt;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	public int getPrePage() {
		return prePage;
	}

	public void setPrePage(int prePage) {
		this.prePage = prePage;
	}

	public int getNextPage() {
		return nextPage;
	}

	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	public int getCriterion() {
		return criterion;
	}

	public void setCriterion(int criterion) {
		this.criterion = criterion;
	}

	public String getSearchCondition() {
		return searchCondition;
	}

	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	@Override
	public String toString() {
		return "Pagination [pageSize=" + pageSize + ", blockSize=" + blockSize + ", curPage=" + curPage
				+ ", startPoint=" + startPoint + ", modelCnt=" + modelCnt + ", pageCnt=" + pageCnt + ", startPage="
				+ startPage + ", endPage=" + endPage + ", prePage=" + prePage + ", nextPage=" + nextPage
				+ ", criterion=" + criterion + ", searchCondition=" + searchCondition + ", searchKeyword="
				+ searchKeyword + "]";
	}

	/**
	 * 각 필드를 적절한 값으로 수정한다.
	 * @param pageSize
	 * @param blockSize
	 * @param modelCnt
	 */
	public void modifyPagination(int pageSize, int blockSize, int modelCnt) {
		this.pageSize = pageSize;
		this.blockSize = blockSize;
		this.modelCnt = modelCnt;
		
		// pageCnt
		pageCnt = modelCnt / pageSize;
		if((modelCnt % pageSize) > 0) {
			pageCnt ++;
		}
		
		// curPage
		if(curPage == 0) {
			curPage = pageCnt;
		}
		
		// startPoint
		startPoint = (curPage-1) * pageSize;
		
		// startPage
		startPage = ((curPage - 1)/blockSize) * blockSize + 1;
		
		// endPage
		endPage =  startPage + blockSize - 1;
		if(endPage > pageCnt) {
			endPage = pageCnt;
		}
		
		// prePage
		prePage = getStartPage() - 1;
		if(prePage < 1) {
			prePage = 1;
		}
		
		// nextPage
		nextPage = getEndPage() + 1;
		if(nextPage > getPageCnt()) {
			nextPage = getPageCnt();
		}
	}
	
}
