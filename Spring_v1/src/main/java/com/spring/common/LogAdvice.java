package com.spring.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Component
@Aspect
public class LogAdvice {

	protected Log log = LogFactory.getLog(LogAdvice.class);

	@Pointcut("execution(* com.spring..*(..))")
	public void allPointcut() {	}
	
	@Before("allPointcut()")
	public void beforeLog(JoinPoint jp) {
		String method = jp.getSignature().getName();
		Object[] args = jp.getArgs();
//		System.out.println("[공통로그] 수행 메소드 : " + method + "()");
//		System.out.print("ARGS 정보 : ");
//		for(int i=0; i<args.length; i++)
//			System.out.print("" +args[i].toString() + "\n");
		
//		log.debug("수행 메소드 : " + method + "()");
	}

	
	@Around("allPointcut()")
	public Object aroundLog(ProceedingJoinPoint pjp) throws Throwable {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Object obj = pjp.proceed();
		
		stopWatch.stop();
		log.debug("걸린 시간 : " + stopWatch.getTotalTimeMillis() + "(ms)초");
		
		return obj;
	}
	
		
}
