package com.spring.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.spring.model.User;

@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	protected Log log = LogFactory.getLog(LoginInterceptor.class); 
			
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		User login = (User) request.getSession().getAttribute("login");
		String uri = request.getRequestURI();
		log.debug(login);
		if(uri.equals("/")||uri.equals("/main.do")
				||uri.equals("/getPostList.do")||uri.equals("/checkPost.do")
				||uri.equals("/insertUser.do")||uri.equals("/checkUser.do")
				||uri.equals("/login.do")|| uri.equals("/logout.do")) {
		} else {
			if(login == null) {
				// 세션에 로그인 정보가 없다면 login.do로 이동한다.
				response.sendRedirect("/login.do");
				return false;
			}
		}
		
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}

}
