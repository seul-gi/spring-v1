package com.spring.dao;

import java.util.List;

import com.spring.model.Category;

public interface CategoryDAO {

	// 카테고리 조회
	Category getCategory(Category category);
	
	// 카테고리 목록 조회
	List<Category> getCategoryList();
	
	// 카테고리 등록
	void insertCategory(Category category);
	
	// 카테고리 수정
	void updateCategory(Category category);
	
	// 카테고리 삭제
	void deleteCategory(Category category);
}
