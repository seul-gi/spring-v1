package com.spring.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.model.Category;

@Repository
public class CategoryDAOImpl implements CategoryDAO {

	@Autowired
	private SqlSessionTemplate mybatis;
	
	@Override
	public Category getCategory(Category category) {
		return mybatis.selectOne("CategoryDAO.getCategory", category);
	}

	@Override
	public List<Category> getCategoryList() {
		return mybatis.selectList("CategoryDAO.getCategoryList");
	}

	@Override
	public void insertCategory(Category category) {
		mybatis.insert("CategoryDAO.insertCategory", category);
	}

	@Override
	public void updateCategory(Category category) {
		mybatis.update("CategoryDAO.updateCategory", category);
	}

	@Override
	public void deleteCategory(Category category) {
		mybatis.delete("CategoryDAO.deleteCategory", category);
	}

}
