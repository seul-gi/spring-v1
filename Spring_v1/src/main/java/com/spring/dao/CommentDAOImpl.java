package com.spring.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.common.Pagination;
import com.spring.model.Comment;

@Repository
public class CommentDAOImpl implements CommentDAO {

	@Autowired
	private SqlSessionTemplate mybatis;
	
	@Override
	public Comment getComment(int id) {
		return mybatis.selectOne("CommentDAO.getComment", id);
	}

	@Override
	public List<Comment> getCommentList(Pagination pagination) {
		return mybatis.selectList("CommentDAO.getCommentList", pagination);
	}

	@Override
	public int insertComment(Comment comment) {
		return mybatis.insert("CommentDAO.insertComment", comment);
	}

	@Override
	public int updateComment(Comment comment) {
		return mybatis.update("CommentDAO.updateComment", comment);
	}

	@Override
	public int deleteComment(int id) {
		return mybatis.update("CommentDAO.deleteComment", id);
	}

	@Override
	public int getCommentCnt(Pagination pagination) {
		return mybatis.selectOne("CommentDAO.getCommentCnt", pagination);
	}

	@Override
	public int getCommentSequence(Comment comment) {
		return mybatis.selectOne("CommentDAO.getCommentSequence", comment);
	}

	@Override
	public void updateCommentSequence(Comment comment) {
		mybatis.update("CommentDAO.updateCommentSequence", comment);
	}
	
	@Override
	public void deleteCommentSequence(Comment comment) {
		mybatis.update("CommentDAO.deleteCommentSequence", comment);
	}
	
	@Override
	public int updateCommentChildren(int id) {
		return mybatis.update("CommentDAO.updateCommentChildren", id);
	}
	
	@Override
	public int deleteCommentChildren(int id) {
		return mybatis.update("CommentDAO.deleteCommentChildren", id);
	}
}
