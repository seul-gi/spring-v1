package com.spring.dao;

import java.util.List;

import com.spring.common.Pagination;
import com.spring.model.Comment;

public interface CommentDAO {

	// 코멘트 조회
	Comment getComment(int id);
	
	// 코멘트 목록 조회
	List<Comment> getCommentList(Pagination pagination);
	
	// 코멘트 등록
	int insertComment(Comment comment);
	
	// 코멘트 수정
	int updateComment(Comment comment);
	
	// 코멘트 삭제
	int deleteComment(int id);

	// 코멘트 개수
	int getCommentCnt(Pagination pagination);
		
	// 코멘트 순서 확인
	int getCommentSequence(Comment comment);
	
	// 코멘트 순서 증가
	void updateCommentSequence(Comment comment);
	
	// 코멘트 순서 감소
	void deleteCommentSequence(Comment comment);
	
	// 코멘트 children 증가
	int updateCommentChildren(int id);
	
	// 코멘트 children 감소
	int deleteCommentChildren(int id);
}
