package com.spring.dao;

import java.util.List;

import com.spring.common.Pagination;
import com.spring.model.Post;

public interface PostDAO {

	// post 조회
	Post getPost(int id);
	
	// post 목록 조회
	List<Post> getPostList(Pagination pagination);
	
	// post 등록
	int insertPost(Post post);
	
	// post 수정
	int updatePost(Post post);
	
	// post 삭제
	int deletePost(int id);
	
	// post 개수
	int getPostCnt(Pagination post);
	
	// post 조회수 증가
	int updatePostViews(int id);
	
	// post 순서 확인
	int getPostSequence(Post post);
	
	// post 순서 증가
	int updatePostSequence(Post post);
	
	// post 순서 감소
	int deletePostSequence(Post post);
	
	// post children 증가
	int updatePostChildren(int id);
	
	// post children 감소
	int deletePostChildren(int id);
	
}
