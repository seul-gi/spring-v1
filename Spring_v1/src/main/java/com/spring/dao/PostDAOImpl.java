package com.spring.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.common.Pagination;
import com.spring.model.Post;

@Repository
public class PostDAOImpl implements PostDAO {

	@Autowired
	private SqlSessionTemplate mybatis;
	
	@Override
	public Post getPost(int id) {
		return mybatis.selectOne("PostDAO.getPost", id);
	}

	@Override
	public List<Post> getPostList(Pagination pagination) {
		return mybatis.selectList("PostDAO.getPostList", pagination);
	}

	@Override
	public int insertPost(Post post) {
		return mybatis.insert("PostDAO.insertPost", post);
	}

	@Override
	public int updatePost(Post post) {
		return mybatis.update("PostDAO.updatePost", post);
	}

	@Override
	public int deletePost(int id) {
		return mybatis.update("PostDAO.deletePost", id);
	}

	@Override
	public int getPostCnt(Pagination pagination) {
		return mybatis.selectOne("PostDAO.getPostCnt", pagination);
	}
	
	@Override
	public int updatePostViews(int id) {
		return mybatis.update("PostDAO.updatePostViews", id);
	}
	
	@Override
	public int getPostSequence(Post post) {
		return mybatis.selectOne("PostDAO.getPostSequence", post);
	}
	
	@Override
	public int updatePostSequence(Post post) {
		return mybatis.update("PostDAO.updatePostSequence", post);
	}
	
	@Override
	public int deletePostSequence(Post post) {
		return mybatis.update("PostDAO.deletePostSequence", post);
	}
	
	@Override
	public int updatePostChildren(int id) {
		return mybatis.update("PostDAO.updatePostChildren", id);
	}
	
	@Override
	public int deletePostChildren(int id) {
		return mybatis.update("PostDAO.deletePostChildren", id);
	}
	
}
