package com.spring.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SqlSessionTemplate mybatis;
	
	@Override
	public User login(User user) {
		return mybatis.selectOne("UserDAO.login", user);
	}
	
	@Override
	public User getUser(int id) {
		return mybatis.selectOne("UserDAO.getUser", id);
	}

	@Override
	public List<User> getUserList() {
		return mybatis.selectList("UserDAO.getUserList");
	}

	@Override
	public int insertUser(User user) {
		return mybatis.insert("UserDAO.insertUser", user);
	}

	@Override
	public int updateUser(User user) {
		return mybatis.update("UserDAO.updateUser", user);
	}

	@Override
	public int deleteUser(int id) {
		return mybatis.delete("UserDAO.deleteUser", id);
	}
	
	@Override
	public User checkUser(HashMap<String, Object> map) {
		return mybatis.selectOne("UserDAO.checkUser", map);
	}

}
