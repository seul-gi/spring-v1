package com.spring.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.common.Pagination;
import com.spring.model.Bookmark;

@Repository
public class BookmarkDAOImpl implements BookmarkDAO {

	@Autowired
	private SqlSessionTemplate mybatis;
	
	@Override
	public Bookmark getBookmark(int id) {
		return mybatis.selectOne("BookmarkDAO.getBookmark", id);
	}

	@Override
	public List<Bookmark> getBookmarkList(Pagination pagination) {
		return mybatis.selectList("BookmarkDAO.getBookmarkList", pagination);
	}

	@Override
	public int insertBookmark(Bookmark bookmark) {
		return mybatis.insert("BookmarkDAO.insertBookmark", bookmark);
	}

	@Override
	public int updateBookmark(Bookmark bookmark) {
		return mybatis.update("BookmarkDAO.updateBookmark", bookmark);
	}

	@Override
	public int deleteBookmark(int id) {
		return mybatis.delete("BookmarkDAO.deleteBookmark", id);
	}
	
	@Override
	public int getBookmarkCnt(Pagination pagination) {
		return mybatis.selectOne("BookmarkDAO.getBookmarkCnt", pagination);
	}

}
