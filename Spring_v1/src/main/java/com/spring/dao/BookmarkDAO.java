package com.spring.dao;

import java.util.List;

import com.spring.common.Pagination;
import com.spring.model.Bookmark;

public interface BookmarkDAO {
	
	// 북마크 조회
	Bookmark getBookmark(int id);
	
	// 북마크 목록 조회
	List<Bookmark> getBookmarkList(Pagination pagination);
	
	// 북마크 등록
	int insertBookmark(Bookmark bookmark);
	
	// 북마크 수정
	int updateBookmark(Bookmark bookmark);
	
	// 북마크 삭제
	int deleteBookmark(int id);
	
	// 북마크 개수
	int getBookmarkCnt(Pagination pagination);
	
}
