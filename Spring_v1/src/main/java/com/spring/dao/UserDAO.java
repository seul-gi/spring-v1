package com.spring.dao;

import java.util.HashMap;
import java.util.List;

import com.spring.model.User;

public interface UserDAO {
	
	// 로그인
	User login(User user);
	
	// 회원 조회
	User getUser(int id);
	
	// 회원 목록 조회
	List<User> getUserList();
	
	// 회원 등록
	int insertUser(User user);
	
	// 회원 수정
	int updateUser(User user);
	
	// 회원 삭제
	int deleteUser(int id);
	
	// 회원 검색
	User checkUser(HashMap<String, Object> map);
	
}
