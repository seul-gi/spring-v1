package com.spring.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.common.Pagination;
import com.spring.dao.PostDAO;
import com.spring.model.Post;
import com.spring.model.User;

@Service
public class PostServiceImpl implements PostService {

	@Autowired
	private PostDAO postDAO;
	
	/**
	 * 글 조회
	 * @param id
	 * @return
	 */
	@Override
	public Post getPost(int id) {
		// 조회수 +1
		postDAO.updatePostViews(id);
		return postDAO.getPost(id);
	}
	
	/**
	 * 글을 읽을 수 있는지 체크한다.
	 * @param id
	 * @param login
	 * @return
	 */
	@Override
	public HashMap<String, Object> checkPost(int id, User login) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		Post post = postDAO.getPost(id);
		Post parent = postDAO.getPost(post.getParent_id());
		// 공개글이면 true. 비밀글이면 false.
		boolean open = post.getSecret() == 0 ? true : false;
		// 작성자가 삭제했다면 false.
		boolean status = post.getStatus() == 1 ? true : false;
		// 글 작성자와 로그인한 회원이 같다면 true.
		boolean writer = post.getUser_id() == login.getId();
		// 부모 글 작성자와 로그인한 회원이 같다면 true.
		boolean parent_user = login.getId() == parent.getUser_id();
		// 로그인한 회원 권한이 관리자라면 true.
		boolean admin = login.getRole().equals("admin");
		
		if(status && open	// 공개글
			|| status && !open && writer	// 로그인한 회원이 글 작성자
			|| status && parent_user	// 로그인한 회원이 부모 글 작성자
			|| status && !open && admin) {	// 로그인한 회원이 관리자일 때
			map.put("result", "success");	// 글 열람 가능
		} else if (!status) {
			map.put("result", "failure");	// 삭제글
			map.put("msg", "삭제된 글입니다");
		} else {
			map.put("result", "failure");	// 삭제글
			map.put("msg", "비밀글입니다");	// 비밀글
		}
		return map;
	}

	/**
	 * 글 리스트를 조회한다.
	 * @param paginaiton
	 * @return
	 */
	@Override
	public HashMap<String, Object> getPostList(Pagination pagination) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// curPage 정보가 없을 경우
		if(pagination.getCurPage()==0) {
			// curPage는 1
			pagination.setCurPage(1);
		}
		// searchCondition 정보가 없을 경우
		if(pagination.getSearchCondition() == null || pagination.getSearchCondition().equals("")) {
			pagination.setSearchCondition("title");
			pagination.setSearchKeyword("");
		}
		int modelCnt = postDAO.getPostCnt(pagination);	// 전체 글 수
		int pageSize = 10;	// 한 페이지당 글 수
		int blockSize = 5;	// 한 블록당 페이지 수
		// pagination 적절한 값으로 수정하기
		pagination.modifyPagination(pageSize, blockSize, modelCnt);
		map.put("pagination", pagination);
		

		if(modelCnt > 0) {	// 글이 있다면
			map.put("postList", postDAO.getPostList(pagination));
			map.put("result", "success");
		} else {
			map.put("msg", "등록된 글이 없습니다.");
		}
		return map; 
	}

	/**
	 * 글을 등록한다.
	 * @param post
	 * @return
	 */
	@Override
	public HashMap<String, Object> insertPost(Post post) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// 답글이라면
		if(post.getParent_id()!=0) {
			// 부모 글 정보를 가져와서 parent에 담는다.
			Post parent = postDAO.getPost(post.getParent_id());
			// post에 필요한 값 set
			// category_id. 부모와 동일하게
			post.setCategory_id(parent.getCategory_id());
			// group_id. 부모와 동일하게
			post.setGroup_id(parent.getGroup_id());
			// sequence. getPostSequence() 수행 값으로 설정.
			post.setSequence(postDAO.getPostSequence(post));
			// depth. 부모의 depth + 1
			post.setDepth(parent.getDepth() + 1);
			
			// 동일 그룹 포스트들의 순서 변경
			postDAO.updatePostSequence(post);
			
			boolean flag = true;
			
			// 부모 post들의 children 증가
			while(flag) {
				int child_id = parent.getId();
				int parent_id = parent.getParent_id();
				postDAO.updatePostChildren(child_id);
				if(child_id == parent_id) {
					// 부모와 자녀 글의 id가 일치한다면 while문 수행 종료
					flag = false;
				} else {
					parent = postDAO.getPost(parent_id);
				}
			}
		}
		
		// 글 제목이 50자를 넘으면
		if(post.getTitle().length() > 50) {
			// 50자까지로 잘라서 set
			post.setTitle(post.getTitle().substring(0, 50));
		}
		
		// post 등록
		if(postDAO.insertPost(post)==1) {
			map.put("result", "success");
		} else {
			map.put("msg", "글 등록 실패");
		}
		return map;
	}
	
	/**
	 * 수정할 post 정보(현재)를 담아서 전달한다.
	 * @param id
	 * @return
	 */
	@Override
	public HashMap<String, Object> updatePostView(int id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("post", postDAO.getPost(id));
		return map;
	}
	
	/**
	 * 글을 수정한다.
	 * @param post
	 * @return
	 */
	@Override
	public HashMap<String, Object> updatePost(Post post) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(postDAO.updatePost(post)==1) {	// 글 수정에 성공하면
			map.put("result", "success");
		} else {
			map.put("msg", "글 수정 실패");
		}
		return map;
	}

	/**
	 * 글을 삭제한다.
	 * @param id
	 * @return
	 */
	@Override
	public HashMap<String, Object> deletePost(int id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// 동일 그룹 포스트들의 순서 변경 
		Post post = postDAO.getPost(id);
		
		// post 삭제
		if(postDAO.deletePost(id) == 1) {	// 글 삭제가 성공하고
			// children이 0이면 같은 그룹 post 순서 변경
			if(post.getChildren() == 0) {
				postDAO.deletePostSequence(post);
			}
			
			// 부모 post children 감소
			boolean flag = true;
			while(flag) {
				int parent_id = post.getParent_id();
				if(id == parent_id) {
					flag = false;
				} else {
					postDAO.deletePostChildren(parent_id);
					id = parent_id;
					post = postDAO.getPost(id);
				}
			}
			map.put("result","success");
		} else {
			map.put("msg", "글 삭제 실패");
		}
		return map;
	}

}
