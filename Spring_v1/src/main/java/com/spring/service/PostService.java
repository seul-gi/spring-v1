package com.spring.service;

import java.util.HashMap;

import com.spring.common.Pagination;
import com.spring.model.Post;
import com.spring.model.User;

public interface PostService {

	// post 조회
	Post getPost(int id);
	
	// post 열람 가능 체크
	HashMap<String, Object> checkPost(int id, User login);
	
	// post 목록 조회
	HashMap<String, Object> getPostList(Pagination paginaiton);
	
	// post 등록
	HashMap<String, Object> insertPost(Post post);
	
	// 수정할 post 정보를 담아서 전달
	HashMap<String, Object> updatePostView(int id);
	
	// post 수정
	HashMap<String, Object> updatePost(Post post);
	
	// post 삭제
	HashMap<String, Object> deletePost(int id);
	
}
