package com.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.dao.CategoryDAO;
import com.spring.model.Category;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDAO categoryDAO;
	
	@Override
	public Category getCategory(Category category) {
		return categoryDAO.getCategory(category);
	}

	@Override
	public List<Category> getCategoryList() {
		return categoryDAO.getCategoryList();
	}

	@Override
	public void insertCategory(Category category) {
		categoryDAO.insertCategory(category);
	}

	@Override
	public void updateCategory(Category category) {
		categoryDAO.updateCategory(category);
	}

	@Override
	public void deleteCategory(Category category) {
		categoryDAO.deleteCategory(category);
	}

}
