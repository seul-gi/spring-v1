package com.spring.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.dao.UserDAO;
import com.spring.model.User;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;
	
	@Override
	public HashMap<String, Object> login(User user) {
		// HashMap 객체를 생성하여 map에 담는다.
		HashMap<String, Object> map = new HashMap<String, Object>();
		// login()을 수행하여 리턴하는 User 객체를 login에 담는다.
		User login = userDAO.login(user);
		if(login != null) {		// login이 null이 아니라면
			map.put("result", "success");
			map.put("login", login);
		} else {
			map.put("result", "failure");
		}
		return map;
	}
	
	@Override
	public HashMap<String, Object> getUser(int id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// getUser()를 수행하고 리턴하는 User 객체를 user에 담는다.
		User user = userDAO.getUser(id);
		if(user!= null) {	// user가 null이 아니라면
			map.put("user", user);
			map.put("result", "success");
		} else {
			map.put("msg", "회원 정보를 찾을 수 없습니다");
		}
		return map;
	}

	@Override
	public List<User> getUserList() {
		return userDAO.getUserList();
	}

	@Override
	public HashMap<String, Object> insertUser(User user) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// insertUser()가 성공했다면
		if(userDAO.insertUser(user) == 1) {
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

	@Override
	public HashMap<String, Object> updateUser(User user) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// updateUser()가 성공했다면
		if(userDAO.updateUser(user) == 1) {
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

	@Override
	public HashMap<String, Object> deleteUser(int id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// deleteUser()가 성공했다면
		if(userDAO.deleteUser(id)==1) {
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

	@Override
	public HashMap<String, Object> checkUser(String condition, String keyword, int id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		User login = null;
		
		if(id != 0) {	// 로그인 중이라면
			// 로그인한 회원의 정보를 가져온다.
			login = userDAO.getUser(id);
		}
		
		if(keyword!="") {	// 키워드를 입력했을 때 수행
			// condition과 keyword를 HashMap에 답아서 DAO의 checkUser 수행
			map.put("condition", condition);
			map.put("keyword", keyword);
			User user = userDAO.checkUser(map);
			
			if(user != null){
				// 이미 사용자가 있다면 사용 불가
				map.put("result", "already");
				
				if(login != null && (login.getEmail().equals(user.getEmail()) || login.getName().equals(user.getName()))) {
					// 로그인한 회원이 현재 자신의 이메일이나 닉네임과 동일한 값을 입력했을 때
					map.put("result", "own");
				}
			} else {
				// 아직 사용자가 없다면 사용 가능
				map.put("result", "yet");
			}
			
		} else {	// 키워드가 없다면 
			map.put("result", "error");
		}
		
		return map;
	}
	
	@Override
	public HashMap<String, Object> checkPassword(int id, String password) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// getUser()의 리턴 값인 User 객체를 user에 담는다.
		User user = userDAO.getUser(id);
		// user의 비밀번호와 입력한 비밀번호가 일치한다면
		if(user.getPassword().equals(password)) {
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}
	
}
