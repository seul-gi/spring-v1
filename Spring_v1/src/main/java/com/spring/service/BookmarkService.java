package com.spring.service;

import java.util.HashMap;

import com.spring.common.Pagination;
import com.spring.model.Bookmark;

public interface BookmarkService {

	// 북마크 목록 조회
	HashMap<String, Object> getBookmarkList(Pagination pagination);
	
	// 북마크 등록
	HashMap<String, Object> insertBookmark(Bookmark bookmark);
	
	// 북마크 수정
	HashMap<String, Object> updateBookmark(Bookmark bookmark);
	
	// 북마크 삭제
	HashMap<String, Object> deleteBookmark(int id);
}
