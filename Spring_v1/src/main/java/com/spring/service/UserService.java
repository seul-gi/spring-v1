package com.spring.service;

import java.util.HashMap;
import java.util.List;

import com.spring.model.User;

public interface UserService {
	
	// 로그인
	HashMap<String, Object> login(User user);
	
	// 회원 조회
	HashMap<String, Object> getUser(int id);
	
	// 회원 목록 조회
	List<User> getUserList();
	
	// 회원 등록
	HashMap<String, Object> insertUser(User user);
	
	// 회원 수정
	HashMap<String, Object> updateUser(User user);
	
	// 회원 삭제
	HashMap<String, Object> deleteUser(int id);
	
	// 회원 검색
	HashMap<String, Object> checkUser(String condition, String keyword, int id);
	
	// 회원이 입력한 비밀번호가 맞는지 확인
	HashMap<String, Object> checkPassword(int id, String password);
}
