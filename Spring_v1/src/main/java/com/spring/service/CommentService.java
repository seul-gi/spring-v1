package com.spring.service;

import java.util.HashMap;

import com.spring.common.Pagination;
import com.spring.model.Comment;

public interface CommentService {

	// 코멘트 목록 조회
	HashMap<String, Object> getCommentList(Pagination pagination);
	
	// 코멘트 등록
	HashMap<String, Object> insertComment(Comment comment);
	
	// 코멘트 수정
	int updateComment(Comment comment);
	
	// 코멘트 삭제
	HashMap<String, Object> deleteComment(int id);
	
}
