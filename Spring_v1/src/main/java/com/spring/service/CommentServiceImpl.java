package com.spring.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.common.Pagination;
import com.spring.dao.CommentDAO;
import com.spring.model.Comment;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	private CommentDAO commentDAO;
	
	@Override
	public HashMap<String, Object> getCommentList(Pagination pagination) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		// 요청한 post_id에 해당하는 댓글 개수
		int modelCnt = commentDAO.getCommentCnt(pagination);
		System.out.println("modelCnt : " + modelCnt);
		if(modelCnt >= 1) {
			int pageSize = 10;	// 한 페이지당 댓글 수
			int blockSize = 5;	// 한 블록당 페이지 수
			// pagination 적절한 값으로 수정하기
			pagination.modifyPagination(pageSize, blockSize, modelCnt);
			map.put("commentList", commentDAO.getCommentList(pagination));
			map.put("cPagination", pagination);
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

	@Override
	public HashMap<String, Object> insertComment(Comment comment) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		// 대댓글 처리
		if(comment.getParent_id()!=0) {
			Comment parent = commentDAO.getComment(comment.getParent_id());
			boolean flag = true;
			// comment에 필요한 값 set
			comment.setGroup_id(parent.getGroup_id());
			comment.setDepth(parent.getDepth() + 1);
			comment.setSequence(commentDAO.getCommentSequence(comment));
			
			// 동일 그룹 댓글들의 순서 변경
			commentDAO.updateCommentSequence(comment);
			
			while(flag) {
				int child = parent.getId();
				int parent_id = parent.getParent_id();
				// parent_id 댓글의 children 증가
				commentDAO.updateCommentChildren(child);
				if(child == parent_id) {
					flag = false;
				} else {
					parent = commentDAO.getComment(parent_id);
				}
			}
		}
		
		// 댓글 등록
		if(commentDAO.insertComment(comment) == 1) {
			map.put("result", "success");
		}
				
		return map;
	}

	@Override
	public int updateComment(Comment comment) {
		return commentDAO.updateComment(comment);
	}

	@Override
	public HashMap<String, Object> deleteComment(int id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		Comment comment = commentDAO.getComment(id);
		boolean flag = true;
		
		// 댓글 삭제 성공 시 수행
		if(commentDAO.deleteComment(id) == 1) {
			
			// 댓글의 children이 없다면
			if(comment.getChildren() == 0) {
				// 동일 그룹 댓글들의 순서 변경
				commentDAO.deleteCommentSequence(comment);
			}
			
			// 부모 댓글의 children 감소
			while(flag) {
				int parent = comment.getParent_id();
				if(id == parent) {
					flag = false;
				} else {
					commentDAO.deleteCommentChildren(parent);
					id = parent;
					comment = commentDAO.getComment(id);
				}
			}
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

}
