package com.spring.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.common.Pagination;
import com.spring.dao.BookmarkDAO;
import com.spring.model.Bookmark;

@Service
public class BookmarkServiceImpl implements BookmarkService {

	@Autowired
	private BookmarkDAO bookmarkDAO;

	@Override
	public HashMap<String, Object> getBookmarkList(Pagination pagination) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(pagination.getSearchKeyword()==null || pagination.getSearchKeyword().equals("")) {
			pagination.setSearchKeyword("");
		}
		// 북마크 개수 구하기
		int modelCnt = bookmarkDAO.getBookmarkCnt(pagination);
		if(modelCnt >= 1) {
			int pageSize = 10;
			int blockSize = 3;
			// pagination 적절한 값으로 수정하기		
			pagination.modifyPagination(pageSize, blockSize, modelCnt);
			// 북마크 리스트 조회해서 HashMap으로
			map.put("bookmarkList", bookmarkDAO.getBookmarkList(pagination));
			map.put("bPagination", pagination);
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

	@Override
	public HashMap<String, Object> insertBookmark(Bookmark bookmark) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(bookmarkDAO.insertBookmark(bookmark)==1) {
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

	@Override
	public HashMap<String, Object> updateBookmark(Bookmark bookmark) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(bookmarkDAO.updateBookmark(bookmark)==1) {
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

	@Override
	public HashMap<String, Object> deleteBookmark(int id) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(bookmarkDAO.deleteBookmark(id)==1) {
			map.put("result", "success");
		} else {
			map.put("result", "failure");
		}
		return map;
	}

}
