/**
 * post 관련 JavaScript, JQuery
 */

$(function(){
	// summernote 초기화
	$('#summernote').summernote({
		tabsize: 2,
		height: 300,
		lang: 'ko-KR'
	});
	
});


/**
 * 회원이 글을 클릭하면 읽을 수 있는지 확인한다.
 * @param id
 * @returns
 */
function checkPost(id){
	var check = false;
	$.ajax({
		type : "GET",
		url : "checkPost.do",
		datatype : "JSON",
		data : {"id" : id},
		async : false,
		success : function(data){
			if(data.result == "success"){
				check = true;
			} else if(data.result == "failure"){
				alert(data.msg);
			} else {
				alert("글 읽기 실패");
			}
		}
	})
	return check;
}

/**
 * 글을 등록한다.
 * @returns
 */
function insertPost(){
	var formData = $("#insertPost").serialize();
	$.ajax({
		type : "POST",
		url : "insertPost.do",
		data : formData,
		cache : false,
		success : function(data){
			if(data.result == "success"){
				location.href = "getPostList.do?criterion="+$("#category_id").val()
			} else {
				alert(data.msg);
			}
		}
	})
	
}

/**
 * 새 글 등록 페이지에서 취소 버튼을 클릭할 때 수행한다. 
 * @param category_id
 * @returns
 */
function cancelInsertPost(category_id){
	if(confirm("글 등록을 취소하고 목록으로 돌아가겠습니까?")){
		location.href="getPostList.do?criterion="+category_id;
	} else {
		return false;
	}
}

/**
 * 글 수정 페이지에서 취소 버튼을 클릭할 때 수행한다.
 * @param category_id
 * @returns
 */
function cancelUpdatePost(category_id){
	if(confirm("글 수정을 취소하겠습니까?")){
		location.href="getPostList.do?criterion="+category_id;
	} else {
		return false;
	}
}

/**
 * 글 조회 페이지에서 삭제 버튼을 클릭할 경우 수행한다.
 * @param id
 * @returns
 */
function checkDeletePost(id, category_id){
	var check = true;
	if(confirm("해당 글을 삭제하겠습니까?")){
		location.href="deletePost.do?id="+id +"&category_id="+category_id;
	} else {
		return false;
	}	
}

/**
 * 북마크에 저장한다.
 * @param post_id
 * @param title
 * @returns
 */
function insertBookmark(id, title){
	console.log("id : " + id);
	console.log("title : " + title);
	$.ajax({
		type : "POST",
		url : "insertBookmark.do",
		datatype : "JSON",
		data : {
			"post_id" : id,
			"title" : title
		},
		success : function(data){
			if(data.result == "success"){
				alert("북마크에 저장했습니다.")
			} else {
				alert("북마크 저장 실패");
			}
		}
	})
}