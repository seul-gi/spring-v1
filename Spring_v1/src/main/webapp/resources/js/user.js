/**
 * user 관련 JavaScript, JQuery
 */

$(function(){
	// .userInfo를 클릭하면 #userMsg의 메시지가 사라진다. 
	$(".userInfo").click(function() {
		$("#userMsg").children().remove();
	  });
});

/**
 * 수행 결과에 따른 메시지를 출력한다.
 * @param result
 * @param msg
 * @returns
 */
function printMsg(result, msg){
	$("#userMsg").children().remove();
	tag = '';
	
	// 성공했다면 초록 메시지 창을, 실패했다면 빨간 메시지 창을 표시한다.
	if(result == "success"){
		tag += '<p class="alert alert-success" role="alert">';
	} else {
		tag += '<p class="alert alert-danger" role="alert">';
	}
	
	tag += msg + '</p>';
	$("#userMsg").append(tag);
	
}

/**
 * 이메일, 닉네임, 비밀번호의 유효성 검사를 수행한다.
 * @param str
 * @returns
 */
function checkValidateData(str){
	var check = true;
	var msg = "";
	
	if(str == "email"){	// 이메일 유효성 검사
		var email = $("#email").val();
		var filter = /^[\w-.+]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
		if(filter.test(email)){
			// 적절하게 입력한 이메일은 통과한다.
		} else if(email.length == 0){	// 이메일을 입력하지 않았을 때
			msg = "이메일을 입력하세요.";
			check = false;
		} else {	// 형식에 맞지 않는 이메일을 입력했을 때
			msg = "이메일 형식에 맞게 입력하세요.";
			check = false;
		}
	} else if(str == "name"){	// 닉네임 유효성 검사
		var name = $("#name").val();
		if(name.length == 0){	// 닉네임을 입력하지 않았을 때
			msg = "닉네임을 입력하세요.";
			check = false;
		} else if(name.search(/\s/) != -1){	// 닉네임이 공백을 포함할 때
			msg = "닉네임은 공백없이 입력하세요.";
			check = false;
		} else if(name.length > 10){	// 닉네임이 11자 이상일 때
			msg = "닉네임은 10자까지 가능합니다.";
			check = false;
		}
	} else if(str=="pw"){	// 비밀번호 유효성 검사
		var pw1 = $("#pw1").val();
		var pw2 = $("#pw2").val();
		if(pw1.length < 8){	// 비밀번호가 8자 미만일 때
			msg = "비밀번호는 8자 이상 입력하세요."
			check = false;
		} else if(pw1 != pw2){	// 비밀번호가 서로 일치하지 않을 때
			msg = "비밀번호가 일치하지 않습니다.";
			check = false;
		} else if(pw1.length > 100){	// 비밀번호가 100자 이상일 때
			msg = "비밀번호는 100자 이하 입력하세요."
				check = false;
		}
	}
	
	if(!check){	// check가 false라면 빨간 창에 메시지를 표시한다.
		printMsg("danger", msg);
	}
	return check;
}

/**
 * 이메일, 닉네임의 중복 여부를 검사한다.
 * @param condition
 * @returns
 */
function checkDuplicateData(condition){
	var keyword;
	var check = false;
	
	// 전달받은 condition에 따라 이메일과 닉네임의 중복 검사를 수행한다.
	if(condition=="email"){
		keyword = $("#email").val();
	} else {
		keyword = $("#name").val();
	}
	
	$.ajax({
		type : "GET",
		url : "checkUser.do",
		datatype : "JSON",
		data : {
			"condition" : condition,
			"keyword" : keyword
		},
		async : false,
		success : function(data){
			if(data.result == "yet"){	// 중복 없으므로 사용 가능.
				check = true;
			} else if(data.result == "own"){	// 현재 사용하는 이메일, 닉네임
				check = true;
			}
		}
	})
	
	// 사용 가능시 true, 불가능시 false를 리턴한다.
	return check;
}

/**
 * 회원가입 페이지의 이메일 입력창 옆에 있는 중복 확인 버튼을 누르면 수행한다.
 * @returns
 */
function checkEmail(){
	if(checkValidateData("email")){	// 이메일 유효성 검사를 통과하면 중복 검사를 수행한다.
		if(checkDuplicateData("email")){	// 중복 여부에 따라 메시지를 출력한다.
			printMsg("success", "사용 가능한 이메일입니다.");
		} else {
			printMsg("danger", "사용할 수 없는 이메일입니다.");
		}
	} 
}

/**
 * 회원가입 페이지의 닉네임 입력창 옆에 있는 중복 확인 버튼을 누르면 수행한다.
 * @returns
 */
function checkName(){
	if(checkValidateData("name")){	// 닉네임 유효성 검사를 통과하면 중복 검사를 수행한다.
		if(checkDuplicateData("name")){	// 중복 여부에 따라 메시지를 출력한다.
			printMsg("success", "사용 가능한 닉네임입니다.");
		} else {
			printMsg("danger", "사용할 수 없는 닉네임입니다.");
		}
	}
}

/**
 * 회원가입 페이지에서 회원가입 버튼을 누르면 수행한다.
 * @returns
 */
function insertUser(){
	var check = true;
	
	// 입력한 이메일, 닉네임, 비밀번호의 유효성 및 중복 검사를 수행하여 통과하면 insertUser.do 수행
	if(!checkValidateData("email")){
		check = false;
	} else if(!checkDuplicateData("email")){
		printMsg("danger", "사용할 수 없는 이메일입니다.");
		check = false;
	} else if(!checkValidateData("name")){
		check = false;
	} else if(!checkDuplicateData("name")){
		printMsg("danger", "사용할 수 없는 닉네임입니다.");
		check = false;
	} else if(!checkValidateData("pw")){
		check = false;
	}
	
	if(check){
		$.ajax({
			type : "POST",
			url : "insertUser.do",
			datatype : "JSON",
			data : {
				"email" : $("#email").val(),
				"name" : $("#name").val(),
				"password" : CryptoJS.SHA256($("#pw1").val()).toString()
			},
			success : function(data){
				if(data.result == "success"){
					alert("가입 성공! 로그인 페이지로 이동합니다.");
					location.href = "/login.do";
				} else {
					printMsg("danger", "회원가입에 실패했습니다.");
				}
			}
		})
	}
}

/**
 * 로그인 페이지에서 로그인 버튼을 클릭하면 수행한다.
 * @returns
 */
function login(){
	var check = true;
	var email = $("#email").val();
	var pw = $("#pw").val();
	
	if(!checkValidateData("email")){	// 이메일 유효성 검사 수행
		check = false;
	} else if(pw.length == 0){	// 비밀번호를 입력하지 않으면 메시지 표시
		printMsg("danger", "비밀번호를 입력하세요.");
		check = false;
	}
	
	// 이메일과 비밀번호를 입력했다면 login.do
	if(check){
		$.ajax({
			type : "POST",
			url : "login.do",
			datatype : "JSON",
			data : {
				"email" : email,
				"password" : CryptoJS.SHA256(pw).toString()
				},
			async : false,
			success : function(data){
				if(data.result == "success"){
					// 로그인 성공했다면 메인으로 이동한다.
					location.href = "/main.do";
				} else {
					// 실패했다면 메시지 출력. 페이지 이동은 없음.
					printMsg("danger", "회원정보를 찾을 수 없습니다.");
					check = false;
				}
			}
		})
	}
	
	return check;
}

/**
 * 입력한 비밀번호가 맞는지 확인한다.
 * @returns
 */
function checkPassword(){
	var check = true;
	var pw = $("#oldPw").val();
	// 비밀번호를 입력하지 않았다면
	if(pw.length == 0){
		printMsg("danger", "현재 비밀번호를 입력하세요.");
		check = false;
	} else {
		$.ajax({
			type : "POST",
			url : "checkPassword.do",
			datatype : "JSON",
			data : {"password" : CryptoJS.SHA256(pw).toString()},
			async : false,
			success : function(data){
				if(data.result == "success"){
					// 현재 비밀번호와 일치합니다.
				} else {
					printMsg("danger", "현재 비밀번호가 정확하지 않습니다.")
					check = false;
				}
			}
		})
	}
	
	return check;
}

/**
 * 회원 정보를 수정한다.
 * @returns
 */
function updateUser(){
	var check = true;
	var pw =  $("#oldPw").val()
	var pw1 = $("#pw1").val();
	
	// 입력한 이메일, 닉네임, 새 비밀번호의 유효성 및 중복 검사를 수행하여 통과하면 insertUser.do 수행
	if(!checkValidateData("email")){	// 이메일의 유효성 및 중복 검사
		check = false;
	} else if(!checkDuplicateData("email")){
		printMsg("danger", "사용할 수 없는 이메일입니다.");
		check = false;
	} else if(!checkValidateData("name")){	// 닉네임의 유효성 및 중복 검사
		check = false;
	} else if(!checkDuplicateData("name")){
		printMsg("danger", "사용할 수 없는 닉네임입니다.");
		check = false;
	} else if(!checkPassword()){	// 현재 비밀번호 일치 여부
		check = false;
	}
	
	if(pw1.length > 0){	// 새 비밀번호를 입력했다면
		pw = pw1;	// pw에 pw1(새 비밀번호)값을 담는다.
		if(!checkValidateData("pw")){	// 새 비밀번호 유효성 검사
			check = false;
		}
	}
	
	// check가 true면 updateUser.do
	if(check){
		$.ajax({
			type : "POST",
			url : "updateUser.do",
			datatype : "JSON",
			data : {
				"email" : $("#email").val(),
				"name" : $("#name").val(),
				"password" : CryptoJS.SHA256(pw).toString()
			},
			success : function(data){
				if(data.result == "success"){
					location.href = "/getUser.do";
				} else {
					printMsg("danger", "정보 수정에 실패했습니다.");
				}
			}
		})
	}
}

/**
 * 회원 삭제를 수행한다.
 * @returns
 */
function deleteUser(){
	if(confirm("탈퇴하시겠습니까?")){	
		$.ajax({
			type : "POST",
			url : "deleteUser.do",
			datatype : "JSON",
			success : function(data){
				if(data.result == "success"){
					alert("탈퇴했습니다.");
					location.href = "/main.do";
				} else {
					printMsg("danger", "탈퇴에 실패했습니다.");
				}
			}
		})
	}
}