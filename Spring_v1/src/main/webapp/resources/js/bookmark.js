/**
 * bookmark 관련 JavaScript, JQuery
 */

$(function(){
	// 북마크 리스트를 가져오는 getBookmarkList() 메소드를 수행한다.
	getBookmarkList(1);
	
});

/**
 * 북마크 리스트를 가져온다.
 * @param curPage
 * @returns
 */
function getBookmarkList(curPage) {
	console.log("curPage : " + curPage);
	$.ajax({
		type : "GET",
		url : "getBookmarkList.do",
		datatype : "JSON",
		data : {
			"curPage" : curPage
		},
		success : function(data){
			if(data.result == "success"){
				printBookmarkList(data.bookmarkList, data.bPagination, data.login);
				// 북마크 수정창 숨기기
				$(".box-update-bookmark").hide();
			} else {
				printBookmarkPage("북마크에 저장된 글이 없습니다.");
			}
		}
	})
}

/**
 * getBookmarkList()로 가져온 bookmarkList와 pagination을 화면에 표시한다.
 * @param bookmarkList
 * @param bPagination
 * @param login
 * @returns
 */
function printBookmarkList(bookmarkList, bPagination, login){
	$("#box-bookmarkList").children().remove();
	
	var tag = '';
	
	tag += '<div class="bookmark-list-header">';
	// header
	tag += '<div class="header-title">제목</div>';
	tag += '<div class="header-edit">수정</div>';
	tag += '<div class="header-delete">삭제</div>';
	tag += '</div>';
	tag += '<hr class="bookmark-header-hr">';
	
	// 북마크
	for(i=0; i<bookmarkList.length; i++) {
		var id = bookmarkList[i].id;
		var post_id = bookmarkList[i].post_id;
		var category = bookmarkList[i].post_category;
		var title = bookmarkList[i].title;
		
		tag += '<div id="box-bookmark' + id + '" class="box-bookmark">';
		tag += '<div class="bookmark-title">';
		tag += '<a id="bookmark-title' + id + '" class="bookmark-title-a" href="getPost.do?id=' + post_id + '&criterion=' + category + '" onclick="return checkBookmark(' + post_id + ')">' + title + '</a>';
		tag += '<a href="getPost.do?id=' + post_id + '&criterion=' + category + '" target="_blank" onclick="return checkBookmark(' + post_id + ')"><i class="fas fa-external-link-alt"></i></a>';
		tag += '</div>';
		tag += '<div class="bookmark-edit"><i class="fas fa-edit" onclick="updateBookmarkView(' + id + ')"></i></div>';
		tag += '<div class="bookmark-delete"><i class="far fa-trash-alt" onclick="deleteBookmark(' + id + ', ' + bPagination.curPage + ')"></i></div>';
		tag += '</div>';
		// 북마크 수정창
		tag += '<div id="box-update-bookmark' + id + '" class="box-update-bookmark">';
		tag += '<input type="text" id="update-bookmark-title' + id + '" class="update-bookmark-title">';
		tag += '<button class="btn btn-outline-success btn-sm btn-update-bookmark" onclick="updateBookmark(' + id + ')">제목 수정</button>';
		tag += '</div>';
		tag += '<hr class="bookmark-hr">';
	}
	
	// 검색창
	tag += '<div class="box-search-bookmark">';
	tag += '<input type="text" id="bookmark-keyword" class="bookmark-keyword">';
	tag += '<button class="btn btn-outline-secondary btn-sm btn-search-bookmark" onclick="searchBookmark()">검색</button>';
	tag += '</div>';
	
	// pagination
	tag += '<nav>';
	tag += '<ul class="pagination justify-content-center">';
	if(bPagination.startPage > 1){
		// 첫 페이지
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getBookmarkList(1)"><i class="fas fa-angle-double-left"></i></a></li>';
	}
	if(bPagination.startPage > bPagination.prePage){
		// 이전 페이지
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getBookmarkList(' + bPagination.prePage + ')"><i class="fas fa-angle-left"></i></a></li>'
	}
	// 범위 내 페이지 나열(숫자)
	for(i = bPagination.startPage; i <= bPagination.endPage; i++){
		if(i == bPagination.curPage){
			tag += '<li class="page-item active"><a class="page-link" href="#" id="bCurPage" onclick="getBookmarkList(' + i + ')"><strong>' + i + '</strong></a></li>';
		} else {
			tag += '<li class="page-item"><a class="page-link" href="#" onclick="getBookmarkList(' + i + ')">' + i + '</a></li>';
		}
	}
	if(bPagination.endPage < bPagination.nextPage){
		// 다음 페이지
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getBookmarkList(' + bPagination.nextPage + ')"><i class="fas fa-angle-right"></i></a></li>';
	}
	
	if(bPagination.endPage < bPagination.pageCnt){
		// 마지막 페이지
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getBookmarkList(' + bPagination.pageCnt + ')""><i class="fas fa-angle-double-right"></i></a></li>';
	}
	tag += '</ul>';
	tag += '</nav>';
	
	$("#box-bookmarkList").append(tag);
	
}

/**
 * 저장된 북마크가 없거나, 검색 결과가 없을 경우 메시지를 표시한다.
 * @param msg
 * @returns
 */
function printBookmarkPage(msg){
	$("#box-bookmarkList").children().remove();
	
	var tag = '';
	
	tag += '<div class="bookmark-list-header">';
	// header
	tag += '<div class="header-title">제목</div>';
	tag += '<div class="header-edit">수정</div>';
	tag += '<div class="header-delete">삭제</div>';
	tag += '</div>';
	tag += '<hr class="bookmark-header-hr">';
	
	// 메시지
	tag += '<h3 class="bookmark-msg">' + msg + '</h3>';
	tag += '<hr class="bookmark-hr">';
	
	// 검색창
	tag += '<div class="box-search-bookmark">';
	tag += '<input type="text" id="bookmark-keyword" class="bookmark-keyword">';
	tag += '<button class="btn btn-outline-secondary btn-sm btn-search-bookmark" onclick="searchBookmark()">검색</button>';
	tag += '</div>';
	
	$("#box-bookmarkList").append(tag);
}

/**
 * 북마크에 저장된 글 제목을 클릭했을 때 로그인한 회원이 읽을 수 있는지 확인한다.
 * @param post_id
 * @returns
 */
function checkBookmark(post_id){
	var check = false;
	$.ajax({
		type : "GET",
		url : "checkPost.do",
		datatype : "JSON",
		data : {"id" : post_id},
		async : false,
		success : function(data){
			if(data.result == "success"){
				check = true;
			} else if(data.result == "secret"){
				alert("비밀글입니다.");
			} else if(data.result == "delete"){
				alert("삭제된 글입니다.");
			} else {
				alert("글 읽기 실패");
			}
		}
	})
	return check;
}

/**
 * 북마크 수정창을 보이게 한다.
 * @param id
 * @returns
 */
function updateBookmarkView(id){
	$("#box-update-bookmark"+id).show();
}

/**
 * 북마크 제목을 수정한다.
 * @param id
 * @param post_id
 * @returns
 */
function updateBookmark(id){
	var title = $("#update-bookmark-title"+id).val();
	$.ajax({
		type : "POST",
		url : "updateBookmark.do",
		datatype : "JSON",
		data : {
			"id" : id,
			"title" : title
		},
		success : function(data){
			if(data.result == "success"){
				$("#update-bookmark-title"+id).val("");
				$("#box-update-bookmark"+id).hide();
				$("#bookmark-title"+id).html(title);
			} else {
				alert("북마크 제목 수정 실패");
			}
		}
	})
}

/**
 * 북마크를 삭제한다.
 * @param id
 * @param curPage
 * @returns
 */
function deleteBookmark(id, curPage){
	var check = true;
	if(confirm("북마크를 삭제하겠습니까?")){
		$.ajax({
			type : "POST",
			url : "deleteBookmark.do",
			datatype : "JSON",
			data : {"id" : id},
			success : function(data){
				if(data.result == "success"){
					getBookmarkList(curPage)
				} else {
					alert("북마크 삭제 실패");
				}
			}
		})
	} else {
		check = false;
	}
	return check;
}

/**
 * 북마크 제목으로 검색한다.
 * @returns
 */
function searchBookmark(){
	var title = $("#bookmark-keyword").val();
	$.ajax({
		type : "GET",
		url : "getBookmarkList.do",
		datatype : "JSON",
		data : {
			"curPage" : 1,
			"searchKeyword" : title
		},
		success : function(data){
			if(data.result == "success"){
				printBookmarkList(data.bookmarkList, data.bPagination, data.login);
				// 북마크 수정창 숨기기
				$(".box-update-bookmark").hide();
			} else {
				printBookmarkPage("검색 결과가 없습니다.");
			}
		}
		
	})
}