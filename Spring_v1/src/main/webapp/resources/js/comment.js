/**
 * post의 댓글 관련 JavaScript, JQuery
 */

$(function(){
	var post_id = $("#post_id").val();
	// 댓글 리스트를 가져오는 getCommentList() 메소드를 수행한다.
	getCommentList(post_id, 0);
	
	// summernote 초기화
	$('.summernote').summernote({
		tabsize: 2,
		height: 200,
		lang: 'ko-KR' // default: 'en-US'
	});
	
});

/**
 * 해당 post의 댓글 리스트를 가져온다.
 * @param post_id
 * @param curPage
 * @returns
 */
function getCommentList(post_id, curPage) {
	$.ajax({
		type : "GET",
		url : "getCommentList.do",
		datatype : "JSON",
		data : {
			"criterion" : post_id,
			"curPage" : curPage
		},
		success : function(data){
			if(data.result == "success"){
				printCommentList(data.commentList, data.cPagination, data.login);
				// 댓글 수정창 숨기기
				$(".box-update-comment").hide();
				// 대댓글 입력창 숨기기
				$(".box-reply-comment").hide();
			} else {
				$("#box-commentList").children().remove();
			}
		}
	})
}

/**
 * getCommentList()로 가져온 commentList와 pagination을 화면에 표시한다.
 * @param commentList
 * @param cPagination
 * @param login
 * @returns
 */
function printCommentList(commentList, cPagination, login) {
	// #box-commentList의 자식 요소 삭제
	$("#box-commentList").children().remove();

	var tag = '';
	
	// tag에 댓글리스트 html 코드를 담는다.
	for(i=0; i<commentList.length; i++) {
		var id = commentList[i].id;
		var depth = (commentList[i].depth - 1) * 20;
		var writer = commentList[i].writer;
		var writerId = commentList[i].user_id;
		var date = new Date(commentList[i].reg_date).toLocaleDateString();
		var content = commentList[i].content;
		var user = login.id;
		var userRole = login.role;
		var post_user = commentList[i].post_user;
		var parent_user = commentList[i].parent_user;
		var status = commentList[i].status == 1 ? true : false;
		var open = commentList[i].secret == 0 ? true : false;
		var edit = user == writerId; 
		var authority = false;
		
		if(user == post_user){
			authority = true;
		} else if(user == writerId){
			authority = true;
		} else if (user == parent_user){
			authority = true;
		} else if (userRole == 'admin'){
			authority = true;
		}
		
		if(i > 0 && depth == 0){
			tag += '<hr>';
		}
		tag += '<div id="box-comment' + id + '" class="box-comment" style="margin-left:' + depth + 'px">';
		tag += '<div id="box-get-comment' + id + '" class="box-get-comment">';
		
		// 일반 댓글 or 읽을 수 있는 비밀 댓글
		if((status && open) || (status && !open && authority)){
			console.log("status && open : " + status && open);
			tag += '<div class="row comment-info">';
			tag += '<div id="comment-info-left' + id + '" class="col-sm-8 comment-info-left">';
			// 최상위 댓글은 ┗를 표시하지 않는다.
			if(depth > 1){
				tag += '<strong>┗</strong>';
			}
			if(!open){
				tag += '<i class="fas fa-lock"></i>';
			}
			tag += '<strong><i class="fas fa-user-edit"></i>' + writer + '</strong>';
			tag += '<span><i class="far fa-calendar-alt"></i>' + date + '</span>';
			tag += '</div>';
			tag += '<div id="comment-info-right' + id + '" class="col-sm-4 comment-info-right">';
			tag += '<button id="btn-reply-comment-view' + id + '" class="btn btn-sm btn-reply-comment-view" onclick="replyCommentView(' + id + ')">대댓글</button>';
			// 댓글 작성자는 수정/삭제 버튼이 보이게 한다.
			if(edit){
				tag += '<button id="btn-update-comment-view' + id + '" class="btn btn-sm btn-update-comment-view" onclick="updateCommentView(' + id + ')">수정</button>';
				tag += '<button id="btn-delete-comment' + id + '" class="btn btn-sm btn-delete-comment" onclick="deleteComment(' + id + ')">삭제</button>';
			}
			tag += '</div>';
			tag += '</div>';
			if(depth == 1){
				tag += '<div id="comment-content' + id + '" class="comment-content" style="margin-left:5px">';
			} else {
				tag += '<div id="comment-content' + id + '" class="comment-content" style="margin-left:25px">';
			}
			
			tag += '<p>' + content + '</p>';
			tag += '</div>';
			
		// 비밀 댓글	
		} else if(status && !open && !authority){
			tag += '<div id="comment-info' + id + '" class="row comment-info">';
			// 최상위 댓글은 ┗를 표시하지 않는다.
			if(depth > 1){
				tag += '<strong>┗ </strong>';
			}
			tag += '<i class="fas fa-lock"></i>';
			tag += '<strong><i class="fas fa-user-edit"></i>익명</strong>';
			tag += '<span><i class="far fa-calendar-alt"></i>' + date + '</span>';
			tag += '</div>';
			if(depth == 1){
				tag += '<div id="comment-content' + id + '" class="comment-content" style="margin-left:5px">';
			} else {
				tag += '<div id="comment-content' + id + '" class="comment-content" style="margin-left:25px">';
			}
			
			tag += '<p>비밀 댓글입니다.</p>';
			tag += '</div>';
			
		// 삭제 댓글
		} else if (!status){
			tag += '<div id="comment-info' + id + '" class="row comment-info">';
			// 최상위 댓글은 ┗를 표시하지 않는다.
			if(depth > 1){
				tag += '<strong>┗ </strong>';
			}
			if(!open){
				tag += '<i class="fas fa-lock"></i>';
			}
			if(!edit){
				tag += '<strong><i class="fas fa-user-edit"></i>익명</strong>';
			} else{
				tag += '<strong><i class="fas fa-user-edit"></i>' + writer + '</strong>';
			}
			tag += '<span><i class="far fa-calendar-alt"></i>' + date + '</span>';
			tag += '</div>';
			if(depth == 1){
				tag += '<div id="comment-content' + id + '" class="comment-content" style="margin-left:5px">';
			} else {
				tag += '<div id="comment-content' + id + '" class="comment-content" style="margin-left:25px">';
			}
			
			tag += '<p>삭제된 댓글입니다.</p>';
			tag += '</div>';
		}
		
		tag += '</div>';
		
		// 댓글 수정창
		tag += '<div id="box-update-comment' + id + '" class="box-update-comment">';
		tag += '<div class="box-update-content">';
		tag += '<textarea id="update-comment-content' + id + '" class="update-comment-content" rows="3" cols="" required>' + content + '</textarea>';
		tag += '</div>';
		tag += '<div class="box-update-button">';
		tag += '<label for="update-comment-secret' + id + '"><i class="fas fa-lock"></i></label>';
		tag += '<input type="checkbox" id="update-comment-secret' + id + '" class="update-comment-secret" value="1">';
		tag += '<button id="btn-update-comment' + id + '" class="btn btn-sm" onclick="updateComment(' + id + ')" ><span>수정</span></button>';
		tag += '</div>';
		tag += '</div>';
		// 대댓글 입력창
		tag += '<div id="box-reply-comment' + id + '" class="box-reply-comment">';
		tag += '<div class="box-reply-content">';
		tag += '<textarea id="reply-comment-content' + id + '" class="reply-comment-content" rows="5" placeholer="내용" required></textarea>';
		tag += '</div>';
		tag += '<div class="box-reply-button">';
		tag += '<label for="reply-comment-secret' + id + '"><i class="fas fa-lock"></i></label>';
		tag += '<input type="checkbox" id="reply-comment-secret' + id + '" class="reply-comment-secret" value="1">';
		tag += '<button id="btn-reply-comment' + id + '" class="btn btn-sm" onclick="replyComment(' + id + ')" >등록</button>';
		tag += '</div>';
		tag += '</div>';
		tag += '</div>';
	}
	
	// pagination(댓글)
	tag += '<nav>';
	tag += '<ul class="pagination justify-content-center">';

	// 첫 페이지
	if(cPagination.startPage > 1){
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getCommentList(' + cPagination.criterion + ', 1)"><i class="fas fa-angle-double-left"></i></a></li>';
	}
	// 이전 페이지
	if(cPagination.startPage > cPagination.prePage){
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getCommentList(' + cPagination.criterion + ', ' + cPagination.prePage + ')"><i class="fas fa-angle-left"></i></a></li>';
	}
	// 범위 내 페이지 나열(숫자)
	for(i = cPagination.startPage; i <= cPagination.endPage; i++){
		if(i == cPagination.curPage){
			tag += '<li class="page-item active"><a class="page-link" href="#" id="cCurPage" onclick="getCommentList(' + cPagination.criterion + ', ' + i + ')"><strong>' + i + '</strong></a></li>';
		} else {
			tag += '<li class="page-item"><a class="page-link" href="#" onclick="getCommentList(' + cPagination.criterion + ', ' + i + ')">' + i + '</a></li>';
		}
	}
	// 다음 페이지
	if(cPagination.endPage < cPagination.nextPage){
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getCommentList(' + cPagination.criterion + ', ' + cPagination.nextPage + ')"><i class="fas fa-angle-right"></i></a></li>';
	}
	// 마지막 페이지
	if(cPagination.endPage < cPagination.pageCnt){
		tag += '<li class="page-item"><a class="page-link" href="#" onclick="getCommentList(' + cPagination.criterion + ', ' + cPagination.pageCnt + ')""><i class="fas fa-angle-double-right"></i></a></li>';
	}
	tag += '</ul>';
	tag += '</nav>';
	
	// #box-commentList에 tag를 append
	$("#box-commentList").append(tag);
}

/**
 * 대댓글 입력창을 보여/숨겨 준다.
 * @param id
 * @returns
 */
function replyCommentView(id){
	$("#box-reply-comment"+id).toggle();
	
	if ($("#box-reply-comment"+id).is(":visible")){
		$("#reply-comment-content"+id).summernote({
			tabsize: 2,
			height: 200,
			lang: 'ko-KR'
		});
	}
}



/**
 * 댓글 수정창을 보여/숨겨 준다.
 * @param id
 * @returns
 */
function updateCommentView(id){
	$("#box-update-comment"+id).toggle();
	if ($("#box-update-comment"+id).is(":visible")){
		$("#update-comment-content"+id).summernote({
			tabsize: 2,
			height: 200,
			lang: 'ko-KR'
		});
	} 
}

/**
 * 댓글을 등록한다.
 * @param post_id
 * @returns
 */
function insertComment(post_id){
	var secret = $("#insert-comment-secret").prop("checked") ? 1 : 0;
	var content = $("#insert-comment-content").val();
	
	$.ajax({
		type : "POST",
		url : "insertComment.do",
		datatype : "JSON",
		data : {
			"post_id" : post_id,
			"content" : content,
			"secret" : secret
		},
		success : function(data){
			if(data.result == "success"){
				getCommentList(post_id, 0);
				$(".summernote").summernote('reset');
			} else {
				alert("댓글 등록 실패");
			}
		}
		
	})
}

/**
 * 댓글을 수정한다.
 * @param id
 * @returns
 */
function updateComment(id){
	var content = $("#update-comment-content"+id).val();
	var secret = $("#update-comment-secret"+id).prop("checked") ? 1 : 0;
	var post_id = $("#post_id").val();
	var cCurPage = $("#cCurPage").text();
	
	$.ajax({
		type : "POST",
		url : "updateComment.do",
		datatype : "JSON",
		data : {
			"id" : id,
			"content" : content,
			"secret" : secret
		},
		success : function(data){
			if(data.result == "success"){
				getCommentList(post_id, cCurPage);
			} else {
				alert("댓글 수정 실패");
			}
		}
	})
}

/**
 * 댓글을 삭제한다.
 * @param id
 * @returns
 */
function deleteComment(id){
	var post_id = $("#post_id").val();
	var cCurPage = $("#cCurPage").text();
	
	if(confirm("댓글을 삭제하겠습니까?")){
		$.ajax({
			type : "POST",
			url : "deleteComment.do",
			datatype : "JSON",
			data : {"id" : id},
			success : function(data){
				if(data.result == "success"){
					getCommentList(post_id, cCurPage);
				} else {
					alert("댓글 삭제 실패");
				}
			}
			
		})
	}
}

/**
 * 대댓글을 등록한다.
 * @param id
 * @returns
 */
function replyComment(id){
	var content = $("#reply-comment-content"+id).val();
	var secret = $("#reply-comment-secret"+id).prop("checked") ? 1 : 0;
	var post_id = $("#post_id").val();
	var cCurPage = $("#cCurPage").text();
	
	$.ajax({
		type : "POST",
		url : "insertComment.do",
		datatype : "JSON",
		data : {
			"post_id" : post_id,
			"content" : content,
			"parent_id" : id,
			"secret" : secret
		},
		success : function(data){
			if(data.result == "success"){
				getCommentList(post_id, cCurPage);
			} else {
				alert("대댓글 등록 실패");
			}
		}
	})
	
	
}