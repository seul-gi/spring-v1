<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
<c:if test="${pagination.criterion eq 1}">공지</c:if>
<c:if test="${pagination.criterion eq 2}">자유</c:if>
<c:if test="${pagination.criterion eq 3}">문의</c:if>
게시판 | Spring Is Coming</title>
</head>
<body>
	<!-- 글 목록 페이지 시작 -->
	<!-- 글 목록 -->
	<table class="table table-hover">
		<thead>
			<tr>
				<th class="post-list-title">제목</th>
				<th class="post-list-writer">글쓴이</th>
				<th class="post-list-regDate">작성일</th>
				<th class="post-list-views">조회수</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${postList }" var="post">
				<tr>
					<td>
						<c:if test="${post.notice eq 1 }"><strong></c:if>
						<c:if test="${post.id ne post.parent_id }">
							<c:forEach begin="2" end="${post.depth }">
								&emsp;
							</c:forEach>
							┗&nbsp;
						</c:if>
						<c:if test="${post.secret eq 1 }">
							<i class="fas fa-lock pr-1"></i>
						</c:if>
						<c:if test="${post.status eq 0 }">
						<a>삭제된 글입니다.</a>
						</c:if>
						<c:if test="${post.status eq 1 }">
						<a href="getPost.do?id=${post.id }&criterion=${pagination.criterion}&curPage=${pagination.curPage }&searchCondition=${pagination.searchCondition }&searchKeyword=${pagination.searchKeyword }" onclick="return checkPost(${post.id })">${post.title } (${post.comment_count })</a>
						</c:if>
						<c:if test="${post.notice eq 1 }"></strong></c:if>
					</td>
					<c:if test="${post.status eq 0 }">
						<td class="text-center">익명</td>
					</c:if>
					<c:if test="${post.status eq 1 }">
						<td class="text-center">${post.writer }</td>
					</c:if>
					<td class="text-center">${post.reg_date }</td>
					<td class="text-center">${post.views }</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<!-- 버튼 -->
	<div class="row my-3">
		<div class="col-sm-6 post-button-left">
			<button class="btn btn-sm btn-outline-secondary" onclick="location.href='getPostList.do?criterion=${pagination.criterion}'">목록</button>
		</div>
		<div class="col-sm-6 post-button-right">
			<c:if test="${pagination.criterion eq 2 || pagination.criterion eq 3 || login.role eq 'admin'}">
				<button class="btn btn-sm btn-outline-success" onclick="location.href='insertPost.do?category_id=${pagination.criterion }'">글쓰기</button>
			</c:if>
		</div>
	</div>
	<!-- 검색창-->
	<form action="getPostList.do" method="get">
	<div class="post-search">
		<input type="hidden" name="criterion" value="${pagination.criterion }">
		<input type="hidden" name="curPage" value=1>
		<select name="searchCondition">
			<option value="title">제목</option>
  			<option value="content">내용</option>
		</select>
		<input type="text" name="searchKeyword">
		<input type="submit" class="btn btn-outline-secondary btn-sm" value="검색">
	</div>
	</form>
	<!-- pagination 시작 -->
	<nav>
	<ul class="pagination justify-content-center">
		<!-- 첫 페이지 -->
		<c:if test="${pagination.startPage > 1 }">
			<li class="page-item"><a class="page-link" href="getPostList.do?criterion=${pagination.criterion }&curPage=1"><i class="fas fa-angle-double-left"></i></a></li>
		</c:if>
		<!-- 이전 페이지 -->
		<c:if test="${pagination.startPage > pagination.prePage }">
			<li class="page-item"><a class="page-link" href="getPostList.do?criterion=${pagination.criterion }&curPage=${pagination.prePage}"><i class="fas fa-angle-left"></i></a></li>
		</c:if>
		<!-- 범위 내 페이지 나열(숫자) -->	
		<c:forEach begin="${pagination.startPage }" end="${pagination.endPage }" var="page">
			<c:choose>
				<c:when test="${pagination.curPage eq page}">
					<li class="page-item active"><a class="page-link" href="getPostList.do?criterion=${pagination.criterion }&curPage=${page}"><strong>${page}</strong></a></li>
				</c:when>
				<c:otherwise>
					<li class="page-item"><a class="page-link" href="getPostList.do?criterion=${pagination.criterion }&curPage=${page}">${page}</a></li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<!-- 다음 페이지 -->
		<c:if test="${pagination.endPage < pagination.nextPage  }">
			<li class="page-item"><a class="page-link" href="getPostList.do?criterion=${pagination.criterion }&curPage=${pagination.nextPage}"><i class="fas fa-angle-right"></i></a></li>
		</c:if>		
		<!-- 마지막 페이지 -->
		<c:if test="${pagination.endPage < pagination.pageCnt }">
			<li class="page-item"><a class="page-link" href="getPostList.do?criterion=${pagination.criterion }&curPage=${pagination.pageCnt}"><i class="fas fa-angle-double-right"></i></a></li>
		</c:if>
	</ul>
	</nav>
	<!-- pagination 끝 -->
	<!-- 글 목록 페이지 끝 -->
</body>
</html>