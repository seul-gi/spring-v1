<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${post.title } | Spring Is Coming</title>
<!-- comment.js -->
<script src="resources/js/comment.js"></script>
</head>
<body>
	<!-- post -->
	<input type="hidden" id="post_id" value="${post.id }">
	<input type="hidden" id="post_user" value="${post.user_id }">
<%-- 	<input type="hidden" id="user_id" name="user_id" value="${login.id }"> --%>
	<div class="post-title mw-100 mb-3">
		<c:if test="${post.secret eq 1 }">
			<i class="fas fa-lock pr-1"></i>
		</c:if>
		<strong>${post.title }</strong>
	</div>
	<div class="post-info">
		<span><i class="fas fa-user-edit"></i>${post.writer }</span>
		<span><i class="far fa-calendar-alt"></i>${post.reg_date }</span>
		<span><i class="fas fa-eye"></i>${post.views }</span>
	</div>
	<div class="post-content">${post.content }</div>
	<div class="row">
		<div class="col-sm-6 post-button-left">
			<button class="btn btn-outline-secondary btn-sm" onclick="location.href='getPostList.do?criterion=${pagination.criterion}&curPage=${pagination.curPage }&searchCondition=${pagination.searchCondition }&searchKeyword=${pagination.searchKeyword }'">목록</button>
		</div>
		<div class="col-sm-6 post-button-right">
				<button class="btn btn-outline-info btn-sm" onclick="insertBookmark(${post.id}, '${post.title}' )">북마크</button>
			<c:if test="${post.category_id eq 3 && post.notice eq 0}">
<%-- 				<button class="btn btn-outline-success btn-sm" onclick="location.href='insertRePost.do?id=${post.id}'">답글쓰기</button> --%>
				<button class="btn btn-outline-success btn-sm" onclick="location.href='insertPost.do?category_id=${pagination.criterion }&parent_id=${post.id}'">답글쓰기</button>
			</c:if>
			<c:if test="${login.id eq post.user_id || login.role eq 'admin'}">
				<button class="btn btn-outline-warning btn-sm" onclick="location.href='updatePost.do?id=${post.id}'">수정</button>
				<button class="btn btn-outline-danger btn-sm" onclick="return checkDeletePost(${post.id},${post.category_id})">삭제</button>
			</c:if>
		</div>
	</div>
	<hr>
	<!-- 댓글 리스트 -->
	<div id="box-commentList"></div>
	<!-- 새 댓글 입력 -->
	<div id="box-insert-comment" class="">
			<div class="insert-comment-content">
				<textarea id="insert-comment-content" class="summernote" name="content" rows="5" placeholder="댓글을 입력하세요." required></textarea>
			</div>
			<div class="btn-insert-comment" >
				<label for="secret"><i class="fas fa-lock" title="비밀댓글"></i></label>
				<input type="checkbox" id="insert-comment-secret" name="secret" value="1">
				<button class="btn btn-sm" onclick="insertComment(${post.id })">댓글 등록</button>
			</div>
	</div>
</body>
</html>