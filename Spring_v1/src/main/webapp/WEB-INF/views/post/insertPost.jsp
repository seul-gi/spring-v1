<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>새 글 등록 | Spring Is Coming</title>
</head>
<body>
<!-- 새 글 등록 페이지 시작 -->
<form id="insertPost">
	<input type="hidden" id="category_id" name="category_id" value="${category_id }">
	<input type="hidden" name="parent_id" value="${parent_id }">
	<div class="post-option">
		<c:if test="${category_id eq 3 }">
			<!-- 비밀글 선택 -->
			<label for="secret"><i class="fas fa-lock" title="비밀글"></i>비밀글</label>
			<input type="checkbox" id="secret" name="secret" value="1">
		</c:if>
		<c:if test="${login.role eq 'admin' }">
			<!-- 글 목록 상단에 표시 선택 -->
			<label for="notice">상단</label>
			<input type="checkbox" id="notice" name="notice" value="1">
		</c:if>
	</div>
	<!-- 제목 입력 -->
	<div class="post-title">
		<input class="form-control" type="text" id="insert-post-title" name="title" placeholder="제목" required>
	</div>
	<!-- 내용 입력 -->
	<div class="post-content">
		<textarea id="summernote" name="content" required></textarea>
	</div>
	<!-- 버튼 -->
	<div class="post-button">
		<input type="submit" class="btn btn-outline-success btn-sm" value="등록" onclick="return insertPost()">
		<button class="btn btn-outline-danger btn-sm" onclick="return cancelInsertPost(${category_id })">취소</button>
	</div>
</form>
<!-- 새 글 등록 페이지 끝 -->
</body>
</html>