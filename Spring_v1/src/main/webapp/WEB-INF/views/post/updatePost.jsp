<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>글 수정 | Spring Is Coming</title>
</head>
<body>
<!-- 글 수정 페이지 시작 -->
<form action="updatePost.do" method="post">
	<input type="hidden" name="id" value="${post.id }">
	<input type="hidden" name="category_id" value="${post.category_id }">
	<div class="post-option">
		<c:if test="${post.category_id eq 3 }">
			<!-- 비밀글 선택 -->
			<label for="secret"><i class="fas fa-lock" title="비밀글"></i>비밀글</label>
			<input type="checkbox" id="secret" name="secret" value="1">
		</c:if>
		<c:if test="${login.role eq 'admin' }">
			<!-- 글 목록 상단에 표시 -->
			<label for="notice">상단</label>
			<input type="checkbox" id="notice" name="notice" value="1">
		</c:if>
	</div>
	<!-- 제목 입력 -->
	<div class="post-title">
		<input class="form-control" type="text" name="title" value="${post.title}" required>
	</div>
	<!-- 내용 입력 -->
	<div class="post-content">
		<textarea id="summernote" name="content" required>${post.content }</textarea>
	</div>
	<!-- 버튼 -->
	<div class="post-button">
		<input type="submit" class="btn btn-outline-success btn-sm" value="수정">
		<button class="btn btn-outline-danger btn-sm" onclick="return cancelUpdatePost(${post.category_id })">취소</button>
	</div>
</form>
<!-- 글 수정 페이지 끝 -->
</body>
</html>