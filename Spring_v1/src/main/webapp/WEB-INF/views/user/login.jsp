<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>로그인 | Spring Is Coming</title>
<!-- crypto-js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
<script src="resources/js/user.js"></script>
</head>
<body>
	<div id="box-login" class="box-user">
		<h2>로그인</h2>
		<!-- 메시지 표시 -->
		<div id="userMsg"></div>
		<!-- 이메일 -->
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">이메일</div>
				</div>
				<input type="text" class="form-control userInfo" id="email" name="email" required>
			</div>
		</div>
		<!-- 비밀번호 -->
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">비밀번호</div>
				</div>
				<input type="password" class="form-control userInfo" id="pw" name="password" required>
			</div>
		</div>
		<button class="btn btn-outline-success btn-sm" onclick="login()">로그인</button>
		<button class="btn btn-outline-warning btn-sm" onclick="location.href='insertUser.do'">회원가입</button>
	</div>
</body>
</html>