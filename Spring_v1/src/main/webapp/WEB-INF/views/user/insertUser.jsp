<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원가입 | Spring Is Coming</title>
<!-- crypto-js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
<script src="resources/js/user.js"></script>
</head>
<body>
	<div id="box-insertUser" class="box-user">
		<h2>회원가입</h2>
		<!-- 메시지 표시 -->
		<div id="userMsg"></div>
		<!-- 이메일 -->
		<div class="form-group row">
		<div class="col-sm-9">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">이메일</div>
				</div>
				<input type="text" class="form-control userInfo" id="email" required>
			</div>
		</div>
			<div class="col-sm-3">
				<button class="btn btn-outline-warning btn-sm" onclick="return checkEmail()">중복 확인</button>
			</div>
		</div>
		<!-- 닉네임 -->
		<div class="form-group row">
			<div class="col-sm-9">
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text">닉네임</div>
					</div>
					<input type="text" class="form-control userInfo" id="name" required>
				</div>
			</div>
			<div class="col-sm-3">
				<button class="btn btn-outline-warning btn-sm" onclick="return checkName()">중복 확인</button>
			</div>
		</div>
		<!-- 비밀번호 -->
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">비밀번호</div>
				</div>
				<input type="password" class="form-control userInfo" id="pw1" required>
			</div>
		</div>
		<!-- 비밀번호 확인 -->
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">비밀번호 확인</div>
				</div>
				<input type="password" class="form-control userInfo" id="pw2" required>
			</div>
		</div>
		<button class="btn btn-outline-success btn-sm" onclick="insertUser()">회원가입</button>
	</div>
</body>
</html>