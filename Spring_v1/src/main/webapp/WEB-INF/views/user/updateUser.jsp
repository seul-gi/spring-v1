<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>
<body>
<%-- <div class="user">
	<div class="message mb-3">${message}</div>
	<form action="updateUser.do" method="post">
		<input type="hidden" name="id" value="${login.id }">
		<input type="text" class="form-control mb-3" name="email" value="${login.email }">
		<input type="password" class="form-control mb-3" name="pwd" placeholder="현재 비밀번호">
		<input type="password" class="form-control mb-3" name="password" placeholder="새 비밀번호">
		<input type="password" class="form-control mb-3" placeholder="새 비밀번호확인">
		<input type="text" class="form-control mb-3" name="name" value="${login.name }">
		<input type="hidden" name="role" value="${login.role }">
		<input type="submit" class="btn btn-outline-success btn-sm" value="수정">
	</form>
</div> --%>
	<div class="box-user-info">
		<div class="form-group row">
			<label for="updateUserEmail" class="col-sm-2 col-form-label">이메일</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="updateUserEmail" value="${user.email}" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="updateUserName" class="col-sm-2 col-form-label">닉네임</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="updateUserName" value="${user.name}" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="updateUserOldPassword" class="col-sm-2 col-form-label">현재 비밀번호</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="updateUserOldPassword" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="updateUserPassword" class="col-sm-2 col-form-label">현재 비밀번호</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="updateUserOldPassword" required>
			</div>
		</div>
		<div class="form-group row">
			<label for="updateUserRegDate" class="col-sm-2 col-form-label">가입일</label>
			<div class="col-sm-10">
				<input type="text" readonly class="form-control" id="updateUserRegDate" value="${user.reg_date}">
			</div>
		</div>
		<button class="btn btn-outline-success btn-sm" onclick="location.href='updateUser.do'">정보 수정</button>
		<button class="btn btn-outline-danger btn-sm" onclick="location.href='deleteUser.do?id=${login.id}'">탈퇴</button>
	</div>
</body>
</html>