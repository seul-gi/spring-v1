<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<!DOCTYPE html> 
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="resources/images/spring.ico">
<!-- bootstrap-4.3.1 -->
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
<!-- summernote-bs4 css -->
<link href="resources/summernote/summernote-bs4.css" rel="stylesheet">
<!-- Font Awesome -->
<link rel="stylesheet" href="resources/fontawesome/css/all.css">
<!-- Google Fonts - Nanum Gothic -->
<link href="https://fonts.googleapis.com/css?family=Nanum+Gothic" rel="stylesheet">
<!-- main.css -->
<link rel="stylesheet" href="resources/css/style.css">
<!-- jQuery 3.3.1 -->
<script src="resources/js/jquery-3.3.1.js"></script>
<!-- Popper.js 1.14.7 -->
<script src="resources/js/popper.min.js"></script>
<!-- bootstrap4.3.1 -->
<script src="resources/bootstrap/js/bootstrap.min.js"></script>
<!-- ummernote-bs4 js -->
<script src="resources/summernote/summernote-bs4.min.js"></script>
<!-- summernote-ko-KR -->
<script src="resources/summernote/lang/summernote-ko-KR.js"></script>
<!-- post.js -->
<script src="resources/js/post.js"></script>
</head>
<body>
	<div class="header">
		<nav id="headerNav" class="navbar navbar-expand-sm navbar-dark sticky-top">
		<ul id="headerNav-title" class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="main.do">봄이 온다</a></li>
		</ul>
		<ul id="headerNav-left" class="navbar-nav">
			<li class="nav-item"><a class="nav-link" href="getPostList.do?criterion=1">공지</a></li>
			<li class="nav-item"><a class="nav-link" href="getPostList.do?criterion=2">자유</a></li>
			<li class="nav-item"><a class="nav-link" href="getPostList.do?criterion=3">문의</a></li>
			<li class="nav-item"><a class="nav-link" href="getBookmarkListView.do?">북마크</a></li>
		</ul>
		<ul id="headerNav-right" class="navbar-nav">
			<c:if test="${!empty login.id }">
				<li class="nav-item"><a class="nav-link" href="getUser.do">내정보</a></li>
				<li class="nav-item"><a class="nav-link" href="logout.do">로그아웃</a></li>
			</c:if>
			<c:if test="${empty login.id }">
				<li class="nav-item"><a class="nav-link" href="insertUser.do">회원가입</a></li>
				<li class="nav-item"><a class="nav-link" href="login.do">로그인</a></li>
			</c:if>
		</ul>
		</nav>
	</div>
	<div class="container ">
		<div class="page">
			<c:choose>
				<c:when test="${!empty content }">
					<jsp:include page="${content}" />
				</c:when>
				<c:when test="${param.content != null }">
					<jsp:include page="${param.content}" />
				</c:when>
				<c:otherwise>
					<jsp:include page="/WEB-INF/views/main.jsp" />
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<!-- footer -->
	<div class="footer">
		<span>© 2019 kupulau@naver.com All Rights Reserved</span>
	</div>
</body>
</html>