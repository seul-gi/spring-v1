package com.spring.dao;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spring.model.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class CommentMybatisTest {

	@Autowired
	private CommentDAO dao;
	
//	@Test
	public void getComment() {
//		Comment comment = new Comment();
//		comment.setId(2);
		System.out.println(dao.getComment(2));
	}
	
//	@Test
//	public void getCommentList() {
//		Comment comment = new Comment();
//		comment.setPost_id(2);
//		List<Comment> list = dao.getCommentList(comment);
//		for(Comment p : list)
//			System.out.println(p);
//	}
	
//	@Test
	public void insertComment() {
		Comment comment = new Comment();
		comment.setPost_id(2);
		comment.setUser_id(1);
		comment.setContent("코멘트 내용");
		comment.setSecret(0);
		comment.setParent_id(1);
		dao.insertComment(comment);
	}
	
//	@Test
	public void updateComment() {
		Comment comment = new Comment();
		comment.setId(1);
		comment.setContent("코멘트 내용 수정");
		comment.setSecret(1);
		dao.updateComment(comment);
	}
	
	@Test
	public void deleteComment() {
//		Comment comment = new Comment();
//		comment.setId(2);
		dao.deleteComment(2);
	}
}
