package com.spring.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spring.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class UserMybatisTest {

	@Autowired
	UserDAO dao;
	
	protected Log log = LogFactory.getLog(UserMybatisTest.class); 
	
//	@Test
	public void getUser() {
		User user = new User();
//		user.setEmail("test@email.com");
		System.out.println(dao.getUser(1));
	}
	
	@Test
	public void getUserList() {
		List<User> list = dao.getUserList();
		for(User user : list)
			System.out.println(user);
	}
	
//	@Test
	public void insertUser() {
		User user = new User();
//		EMAIL, PASSWORD, NAME, ROLE
		user.setEmail("user5@emial.com");
		user.setPassword("1234");
		user.setName("회원5");
		user.setRole("user");
		dao.insertUser(user);
	}
	
//	@Test
	public void updateUser() {
		User user = new User();
		user.setId(1);
		user.setEmail("user1@email.com");
		user.setPassword("5678");
		user.setName("회원1");
		user.setRole("user");
		dao.updateUser(user);
	}
	
//	@Test
	public void deleteUser() {
		User user = new User();
//		user.setId(2);
		dao.deleteUser(2);
	}
	
}
