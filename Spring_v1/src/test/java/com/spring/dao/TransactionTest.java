package com.spring.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spring.model.Post;
import com.spring.service.PostService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class TransactionTest {

	@Autowired
	private PostService service;
	
	@Test
	public void insertPost() {
		Post post = new Post();
		post.setId(46);
		post.setCategory_id(2);
		post.setTitle("트랜잭션 테스트 제목");
		post.setContent("테스트 내용");
		post.setUser_id(2);
		service.insertPost(post);
	}
	
}
