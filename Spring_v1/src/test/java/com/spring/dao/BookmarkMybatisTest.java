package com.spring.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spring.model.Bookmark;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class BookmarkMybatisTest {

	@Autowired
	private BookmarkDAO dao;
	
//	@Test
	public void getBookmark() {
//		Bookmark bookmark = new Bookmark();
//		bookmark.setId(1);
		System.out.println(dao.getBookmark(1));
	}
	
	@Test
	public void getBookmarklist() {
//		Bookmark bookmark = new Bookmark();
//		bookmark.setUser_id(1);
//		List<Bookmark> list = dao.getBookmarkList(bookmark);
//		for(Bookmark b : list)
//			System.out.println(b);
	}
	
//	@Test
	public void insertBookmark() {
		Bookmark bookmark = new Bookmark();
		bookmark.setUser_id(1);
		bookmark.setPost_id(1);
		bookmark.setTitle("북마크 제목");
		dao.insertBookmark(bookmark);
	}
	
//	@Test
	public void updateBookmark() {
		Bookmark bookmark = new Bookmark();
		bookmark.setUser_id(2);
		bookmark.setPost_id(1);
		bookmark.setTitle("북마크 제목 수정");
		dao.updateBookmark(bookmark);
	}
	
//	@Test
	public void deleteBookmark() {
//		Bookmark bookmark = new Bookmark();
//		bookmark.setId(4);
		dao.deleteBookmark(4);
	}
	

	
	
}
