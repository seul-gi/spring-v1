package com.spring.dao;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spring.model.Post;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class PostMybatisTest {

	@Autowired
	private PostDAO dao;
	
	@Test
	public void getPost() {
		System.out.println(dao.getPost(2));
	}
	
//	@Test
	public void getPostList() {
		Post post = new Post();
		post.setCategory_id(2);
//		post.setSearchCondition("title");
//		post.setSearchKeyword("2");
//		List<Post> list = dao.getPostList(post);
//		for(Post p : list)
//			System.out.println(p);
	}
	
//	@Test
	public void insertPost() {
		Post post = new Post();
		post.setCategory_id(1);
		post.setTitle("테스트 제목2");
		post.setContent("테스트 내용2");
		post.setUser_id(2);
		post.setSecret(0);
		post.setParent_id(1);
		post.setNotice(1);
		dao.insertPost(post);
	}
	
//	@Test
	public void updatePost() {
		Post post = new Post();
		post.setId(1);
		post.setCategory_id(1);
		post.setTitle("테스트 제목 수정");
		post.setContent("테스트 내용 수정");
		post.setUser_id(1);
		post.setSecret(1);
		post.setNotice(0);
		post.setViews(1);
		post.setComment_count(1);
		dao.updatePost(post);
	}
	
//	@Test
	public void deletePost() {
//		Post post = new Post();
//		post.setId(1);
		dao.deletePost(1);
	}
}
