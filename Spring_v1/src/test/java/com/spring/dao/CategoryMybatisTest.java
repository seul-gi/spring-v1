package com.spring.dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.spring.model.Category;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:src/main/webapp/WEB-INF/spring/root-context.xml")
public class CategoryMybatisTest {

	@Autowired
	private CategoryDAO dao ;
	
//	@Test
	public void getCategory() {
		Category category = new Category();
		category.setId(2);
		System.out.println(dao.getCategory(category));
		
	}
	
	@Test
	public void getCategoryList() {
		List<Category> list = dao.getCategoryList();
		for(Category category : list)
			System.out.println(category);
		
	}
	
//	@Test
	public void insertCategoryList() {
		Category category = new Category();
		category.setSubject("free");
		dao.insertCategory(category);
	}
	
//	@Test
	public void updateCategoryList() {
		Category category = new Category();
		category.setId(2);
		category.setSubject("qna");
		dao.updateCategory(category);
	}
	
//	@Test
	public void deleteCategory() {
		Category category = new Category();
		category.setId(3);
		dao.deleteCategory(category);
	}
}
